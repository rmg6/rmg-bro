package rmg.bro.sql;

import org.junit.jupiter.api.*;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class SqliteJDBCTest {

    private static SqliteJDBC db;
    private static String dbPath;

    @BeforeAll
    public static void setUp() {
        SqliteJDBCTest.db = SqliteJDBC.getInstance();
        dbPath = "out/test/test.db";
    }

    @Test
    public void connectionTest() {
        assertDoesNotThrow(() -> db.getConnection(dbPath));
    }

    @AfterAll
    public static void tearDown() {
        File f = new File(dbPath);
        if(!f.delete()) {
            System.err.println("Could not delete "+dbPath);
        }
    }
}