package rmg.bro.analyzer;

import rmg.bro.dto.Interpretation;
import rmg.bro.dto.RevisionedModels;
import rmg.bro.operators.*;
import rmg.bro.runner.RMGRunner;
import rmg.bro.sql.DAO;
import rmg.bro.sql.exceptions.DbInitException;

import java.sql.SQLException;
import java.util.*;

public class RMGAnalyzer {

    private static final BeliefRevOp D = new Dalal(), S = new Satoh(), W = new Winslett(), F = new Forbus();
    private static final String DALALSATOH = "-dalalSatoh";
    private static final String FORBUSWINSLETT = "-forbusWinslett";
    private static final String KNOWNREL = "-knownRelations";
    private static final String DBFILE = "-db";
    private static final String HELP = "-help";
    private static boolean checkDS;
    private static boolean checkFW;
    private static boolean checkKnownRelations;
    private static DAO dao;

    public static void main(String[] args) {

//        Map<Integer, Set<RevisionedModels>> models = getEvaluations();
        String[] rmgArgs = null, analyzerArgs = null;
        for(int i=0;i<args.length;i++) {
            if(args[i].equals("--")) {
                rmgArgs = new String[args.length-i-1];
                System.arraycopy(args,i+1,rmgArgs,0,rmgArgs.length);

                analyzerArgs = new String[i];
                System.arraycopy(args,0,analyzerArgs,0,analyzerArgs.length);
                break;
            }
        }

        if(analyzerArgs == null) {
            analyzerArgs = args;
            rmgArgs = new String[0];
        }

        if(analyzerArgs.length == 0) {
            System.err.println("Specify analysis.");
            printHelp();
            return;
        }

        Iterator<String> it = Arrays.stream(analyzerArgs).iterator();

        while (it.hasNext()) {
            switch (it.next()) {
                case KNOWNREL -> checkKnownRelations = true;
                case FORBUSWINSLETT -> checkFW = true;
                case DALALSATOH -> checkDS = true;
                case DBFILE -> {
                    if(!it.hasNext()) {
                        System.err.println("Specify a filepath for the database file.");
                        return;
                    }
                    try {
                        dao = new DAO(it.next());
                    } catch (SQLException | ClassNotFoundException | DbInitException e) {
                        e.printStackTrace();
                        return;
                    }
                }
                case HELP -> {
                    printHelp();
                    if(it.hasNext() && it.next().equals("runner")) {
                        RMGRunner.main(new String[]{"-h"});
                    }
                    return;
                }
                default -> {
                    System.err.println("Unknown option.");
                    printHelp();
                    return;
                }
            }
        }

        System.out.println("Running generator...");

        RMGRunner.main(rmgArgs);
        Map<Integer,Set<RevisionedModels>> models = RMGRunner.getEvaluations();


        System.out.println("\n--------------------------------------------------------------------------------");
        System.out.println("Building images...");

        Map<String, Set<Interpretation>> images = null;
        try {
            images = buildImages(models);
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

        System.out.println("Running analysis...");

        if(checkKnownRelations) {
            checkKnownSubsetRelations(images);
        }

        if(checkFW) {
            checkForbusAndWinslett(images);
        }

        if(checkDS) {
            checkDalalAndSatoh(images);
        }

//        System.out.println("Total number of merged models: " + models.size());

    }

    private static void printHelp() {
        System.out.println("Run the program as follows:\n\n\t"
                            + "java rmg/analyzer/RMGAnalyzer <Analysis1> [<Analysis2> ...] [-- <RMGRunnerArgument1> [RMGRunnerArgument2 ...]]\n\n"
                            + "The double dash '--' has to be specified as a separator between the analyzer and runner arguments."
                            + "Available analyer options are:\n\n\t"
                            + KNOWNREL +"\t\tCheck the mathematically proven relations based on generated data.\n\t"
                            + FORBUSWINSLETT +"\t\tCheck the relation between the images of Forbus and Winslett operators.\n\t"
                            + DALALSATOH +"\t\tCheck the relation between the images of Dalal and Satoh operators.\n\t"
                            + DBFILE +" <filepath>\t\tAllows to specify a (sqlite) database file to reuse previous results.\n\t"
                            + HELP + " [runner]" +"\t\tThis help message. The optional argument 'runner' also displays the help message for the runner.");
    }

    public static void checkForbusAndWinslett(Map<String,Set<Interpretation>> images) {
        subsetCheck(images,F.getName(),W.getName());
    }

    public static void checkDalalAndSatoh(Map<String,Set<Interpretation>> images) {
        subsetCheck(images,D.getName(),S.getName());
    }

    public static void subsetCheck(Map<String,Set<Interpretation>> images, String operator1, String operator2) {
        Set<Interpretation> op1 = images.get(operator1),
                op2 = images.get(operator2),
                op1SubOp2 = new HashSet<>(images.get(operator1)),
                op2SubOp1 = new HashSet<>(images.get(operator2));

        op1SubOp2.removeAll(op2);
        boolean isSubset = op1SubOp2.isEmpty();
        System.out.println(operator1+" subset "+ operator2 + ": " + isSubset);

        if(!isSubset) {
            System.out.print("Explanation: ");
            System.out.println(op1SubOp2);
        }

        System.out.println(""); //empty line

        op2SubOp1.removeAll(op1);
        isSubset = op2SubOp1.isEmpty();
        System.out.println(operator2+" subset "+ operator1 + ": " + isSubset);

        if(!isSubset) {
            System.out.print("Explanation: ");
            System.out.println(op2SubOp1);
        }

        System.out.println(""); //empty line
    }

    public static void checkKnownSubsetRelations(Map<String,Set<Interpretation>> images) {
        Set<Interpretation> dalal = images.get(D.getName()),
                            satoh = images.get(S.getName()),
                            winslett = images.get(W.getName()),
                            forbus = images.get(F.getName());

        Set<Interpretation> unionWF = new HashSet<>();
        Set<Interpretation> unionDWF = new HashSet<>();
        Set<Interpretation> unionDS = new HashSet<>();
//                winslett.forEach(p -> unionWF.add(new Interpretation(p)));
        unionWF.addAll(winslett);
        unionWF.addAll(forbus);

        unionDWF.addAll(unionWF);
        unionDWF.addAll(dalal);

        unionDS.addAll(satoh);
        unionDS.addAll(dalal);

        System.out.println("dalal !sub unionWF: " + !unionWF.containsAll(dalal));
        System.out.println("satoh !sub unionDWF: " + !unionDWF.containsAll(satoh));
        System.out.println("forbus !sub unionDS: " + !unionDS.containsAll(forbus));
        System.out.println("winslett !sub unionDS: " + !unionDS.containsAll(winslett));

    }

    private static Map<String,Set<Interpretation>> buildImages(Map<Integer,Set<RevisionedModels>> models) throws SQLException {
        Map<String,Set<Interpretation>> result = new HashMap<>();
        Set<Interpretation> dalal = new HashSet<>(), forbus = new HashSet<>(), winslett = new HashSet<>(), satoh = new HashSet<>();

        if(dao != null) {
            models.put(Collections.max(models.keySet()) + 1, dao.readRevisionResults());
        }

        for (Set<RevisionedModels> m : models.values()) {

            for (RevisionedModels mm : m) {
                if (mm.getOperator().equals(D.getName())) dalal.addAll(mm.getResultModels());
                if (mm.getOperator().equals(S.getName())) satoh.addAll(mm.getResultModels());
                if (mm.getOperator().equals(W.getName())) winslett.addAll(mm.getResultModels());
                if (mm.getOperator().equals(F.getName())) forbus.addAll(mm.getResultModels());
            }
        }
        result.put(D.getName(),dalal);
        result.put(S.getName(),satoh);
        result.put(W.getName(),winslett);
        result.put(F.getName(),forbus);

        return result;
    }
}