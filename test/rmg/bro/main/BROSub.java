package rmg.bro.main;

import rmg.bro.dto.Interpretation;
import rmg.bro.operators.BeliefRevOp;

import java.util.Set;

public class BROSub extends BeliefRevOp {

    public static final String MYOP = "myop";

    @Override
    public Set<Interpretation> apply(Set<Interpretation> kb, Set<Interpretation> mu) {
        Set<Interpretation> inter = null;//super.apply(operator,kb,mu);
        if(inter == null) {
            System.out.println("do myop and return result");
        }

        return inter;
    }

    @Override
    public String getName() {
        return MYOP;
    }
}
