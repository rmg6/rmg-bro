package rmg.bro.util;

import java.util.Objects;

public class Triple<E, F, G> {

    private E fst;
    private F snd;
    private G trd;

    public Triple(E fst, F snd, G trd) {
        this.fst = fst;
        this.snd = snd;
        this.trd = trd;
    }

    public E getFst() {
        return fst;
    }

    public void setFst(E fst) {
        this.fst = fst;
    }

    public F getSnd() {
        return snd;
    }

    public void setSnd(F snd) {
        this.snd = snd;
    }

    public G getTrd() {
        return trd;
    }

    public void setTrd(G trd) {
        this.trd = trd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Triple)) return false;
        Triple<?, ?, ?> triple = (Triple<?, ?, ?>) o;
        return fst.equals(triple.fst) &&
                snd.equals(triple.snd) &&
                trd.equals(triple.trd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fst, snd, trd);
    }
}