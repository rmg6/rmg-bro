package rmg.bro.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MathTest {

    @Test
    public void sumCombinations() {
        assert(Math.sumCombinations(6,3) == 41);
    }

    @Test
    public void binom() {
        assert(Math.binom(1,0) == 1);
        assert(Math.binom(5,5) == 1);
        assert(Math.binom(6,3) == 20);
        assert(Math.binom(15,2) == 105);

        assert(Math.binom(0,1) == 0);
    }

    @Test
    public void fact() {
        assert(Math.fact(1) == 1);
        assert(Math.fact(2) == 2);
        assert(Math.fact(3) == 6);
        assert(Math.fact(4) == 24);
    }

    @Test
    public void sign() {
        assert(Math.sign(-1)==-1);
        assert(Math.sign(1)==1);
        assert(Math.sign(0)==0);
    }
}