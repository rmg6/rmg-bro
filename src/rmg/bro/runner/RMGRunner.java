package rmg.bro.runner;

import rmg.bro.dto.Interpretation;
import rmg.bro.dto.RevisionedModels;
import rmg.bro.dto.ModelConfig;
import rmg.bro.dto.RMGConfig;
import rmg.bro.main.RandomModelGenerator;
import rmg.bro.main.exceptions.InvalidConfigException;
import rmg.bro.runner.exceptions.DirectoryNotCreatedException;
import rmg.bro.runner.exceptions.InvalidArgumentException;
import rmg.bro.main.exceptions.LetterOutOfBoundsException;
import rmg.bro.operators.BeliefRevOp;
import rmg.bro.operators.OperatorFactory;
import rmg.bro.main.HornFunctions;
import rmg.bro.sql.DAO;
import rmg.bro.sql.exceptions.DbInitException;
import rmg.bro.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static rmg.bro.util.Math.sumCombinations;

public class RMGRunner {
    private static final String FRAGMENT = "horn";

    private static Integer cycles;
    private static Integer mergedModelSize;
    private static boolean filterInFragment;
    private static boolean v, vv, quiet;
    private static String delimiter;
    private static boolean writeSeedsToFile;
    private static ConcurrentMap<Integer,Set<RevisionedModels>> evaluations;
    private static String database;

    public static void main(String[] args) {

        cycles = 1;
        mergedModelSize = 0;
        delimiter = ";";
        evaluations = new ConcurrentHashMap<>();

        filterInFragment = true;
        final RMGConfig[] inputConfig;

        try {
            inputConfig = new RMGConfig[]{parse_args(args)};
        } catch (IOException | InvalidArgumentException e) {
            e.printStackTrace();
            return;
        }

        ArrayList<Integer> cycleList = new ArrayList<>(cycles);
        for (int i = 0; i < cycles; i++) {
            cycleList.add(i+1);
        }


//        final int[] notInFragmentCnt = {0};
        Lock lock = new ReentrantLock();

        cycleList.parallelStream().forEach(cycleCnt -> {
//            System.out.println("Cycle "+cycleCnt);

            RMGConfig in = new RMGConfig(inputConfig[0]);
            int mergedModelMinSize = 0;

            Pair<Set<Interpretation>, Queue<Long>> kb, mu;

            Pair<String, Queue<Long>> seeds;

            lock.lock();
            seeds = in.popSeeds();
            lock.unlock();

            try {
                /* NOTE! The seeds for both are kept in the same variable.
                *  The first part of the list is for the kb, the rest for mu.
                *  This is using that non-primitive data types are pass by reference only, so
                *  seeds.getSnd() is the same in the method call and is changed there.
                * */
                kb = RandomModelGenerator.createModels(in.getKb(), seeds.getSnd());
                mu = RandomModelGenerator.createModels(in.getRm(), seeds.getSnd());
            } catch (LetterOutOfBoundsException | InvalidConfigException e) {
                e.printStackTrace();
                return;
            }

            /* Get operator classes according to arguments. */
            Map<String, Set<Interpretation>> result = new HashMap<>();
            for(String op: in.getOperators()) {
                BeliefRevOp operator = OperatorFactory.getOperator(op);

                if (operator != null) {
                    result.put(op,operator.apply(kb.getFst(), mu.getFst()));
                } else {
                    System.err.println("Unknown operator "+op+". Qutting.");
                    return;
                }
            }

            /* Apply operators to generated models. */
            Set<RevisionedModels> evaluation = evaluate(result,kb.getFst(),mu.getFst());

            /* Create output */
            StringBuilder outputString = new StringBuilder(header());
            boolean allInFragment = true;

            for (RevisionedModels mm: evaluation) {
                String operator = mm.getOperator();
                boolean inFragment = mm.isInFragment();

                allInFragment = allInFragment && inFragment;
                mergedModelMinSize += result.get(operator).size() >= mergedModelSize && inFragment ? result.get(operator).size() : 0;
                outputString.append(outputData(operator, inFragment, kb, mu, result.get(operator)));
            }

            /* Output data and store result for  */
            if((!allInFragment || !filterInFragment) && mergedModelMinSize >= mergedModelSize) {
//                if(!allInFragment) {
//                    notInFragmentCnt[0]++;
//                }

                lock.lock();
                if (writeSeedsToFile) {
                    try {
                        writeSeeds(in.getSeedsPath(), cycleCnt, kb.getSnd(), mu.getSnd());
                    } catch (IOException | DirectoryNotCreatedException e) {
                        e.printStackTrace();
                    }
                }
                lock.unlock();


                evaluations.put(cycleCnt,evaluation);
                println(outputString.toString());
            }

//            System.out.println("Finished cycle "+cycleCnt);
        });

        if(database != null) {
            try {
                writeToDb(database);
            } catch (SQLException | ClassNotFoundException | DbInitException throwables) {
                throwables.printStackTrace();
            }
//        println(notInFragmentCnt[0] + "/" + cycles + " merged models are not in specified fragment of propositional logic.");
        }
    }

    private static void writeToDb(String database) throws SQLException, ClassNotFoundException, DbInitException {
        DAO dao = new DAO(database.equals("") ? null : database);
        for(Set<RevisionedModels> values: evaluations.values()) {
            for(RevisionedModels model: values) {
                dao.add(model);
            }
        }
    }

    private static void println(String output) {
        if(!quiet) {
            System.out.println(output);
        }
    }

    public static Map<Integer, Set<RevisionedModels>> getEvaluations() {
        return evaluations;
    }

    public static Set<RevisionedModels> evaluate(Map<String, Set<Interpretation>> revisionResult, Set<Interpretation> kb, Set<Interpretation> mu) {
        Set<RevisionedModels> result = new HashSet<>();

        for(String operator: revisionResult.keySet()) {
            Set<Interpretation> opResult = revisionResult.get(operator);
            //TODO rewrite this to dynamically load class and function specified as parameter to use with other fragments
//            result.put(operator,opResult.equals(HornFunctions.closure(opResult)));
            result.add(new RevisionedModels(operator,FRAGMENT,opResult.equals(HornFunctions.closure(opResult)),kb,mu,opResult));
        }
        return result;
    }

    private static String outputData(String operator,
                                     boolean inFragment,
                                     Pair<Set<Interpretation>, Queue<Long>> kb,
                                     Pair<Set<Interpretation>, Queue<Long>> mu,
                                     Set<Interpretation> result) {
        String output = operator+ delimiter;
        if(v) {
            output += kb.getFst()+ delimiter +mu.getFst()+ delimiter;
        }
        output += result+ delimiter +inFragment;
        if(vv) {
            output += delimiter +kb.getSnd()+ delimiter +mu.getSnd();
        }
//        return "operator;knowledge base (kb);revision models (rm);merged models;horn;kb seeds;rm seeds\n";
        return output+"\n";
    }

    private static String header() {
        String header = "operator"+ delimiter;
        if(v) {
            header += "knowledge base (kb)"+ delimiter +"revision models (rm)"+ delimiter;
        }
        header += "revisions"+ delimiter + FRAGMENT;
        if(vv) {
            header += delimiter +"kb seeds"+ delimiter +"rm seeds";
        }
//        return "operator;knowledge base (kb);revision models (rm);merged models;horn;kb seeds;rm seeds\n";
        return header+"\n";
    }



    private static RMGConfig parse_args(String[] args) throws InvalidArgumentException, IOException {
        ModelConfig kb = new ModelConfig(3,5,20,5);
        ModelConfig rm = new ModelConfig(3,5,20,5);
        Set<String> operators = OperatorFactory.operators();
        Map<String,Queue<Long>> seeds = new HashMap<>();
        String seedsFile = null;
        boolean maxCycles = false;

        ListIterator<String> iter = Arrays.asList(args).listIterator();

        while(iter.hasNext()) {
            String arg = iter.next();
            switch (arg) {
                case "-s" -> seeds = readFiles(getParameter(iter,"Additional parameter required for argument "+arg));
                case "-k" -> kb = readModelConfig(iter);
                case "-r" -> rm = readModelConfig(iter);
                case "-o" -> operators = readOperators(iter);
                case "-c" -> {
                    String next = iter.next();
                    if(next.equals("max")) {
                        maxCycles = true;
                    } else {
                        cycles = Integer.valueOf(next);
                    }
                }
                case "-m" -> mergedModelSize = Integer.valueOf(iter.next());
                case "-d" -> {
                    writeSeedsToFile = true;

                    if(iter.hasNext()) {
                        String str = iter.next();
                        if (str.charAt(0) == '-') {
                            iter.previous();
                        } else {
                            seedsFile = str;
                        }
                    }
                }
                case "-p" -> {
                    database = "";
                    if(iter.hasNext()) {
                        String next = iter.next();
                        if(next.charAt(0) == '-') {
                            iter.previous();
                        } else {
                            database = next;
                        }
                    }
                }
                case "-f" -> filterInFragment = false; //argument to filter out horn results (or conversly to filter them by default and add them explicitly to the result)
                case "-v" -> v = true;
                case "-vv" -> {v = true; vv = true;}
                case "-q" -> quiet = true;
                //TODO add argument for logic fragment
                case "-h" -> {
                    String out = "Run the program as follows:\n\n\t"
                            + "java rmg/bro/main/RMGRunner [Arg1 ...]\n"+"\n\t";
                    out += "-s <seedsPath>" + "\t\t"
                            + "Input file or directory of input files, which hold seeds to reproduce previous outputs.\n\t\t\t\t"
                            + "Each file has to contain one seed per row, no other formatting, and the expected seeds are of Java datatype long.";
                    out += "\n\t" + "-k <knowledgeBaseCfg>" + "\t"
                            + "The config can be either of the following:\n\t\t\t\t"
                            + "- A comma separated ordered list of integers with the definition \n\t\t\t\t  minModelCount,maxModelCount,numberOfAvailableAtoms,maxModelAtomCount (max number of atoms per model)\n\t\t\t\t"
                            + "- A list of key-value pairs: min=<integer> max=<integer> cnt=<integer> mac=<integer>";
                    out += "\n\t" + "-r <revisionModelsCfg>" + "\t" + "Same as -k but for the models used to revise the knowledge base.";
                    out += "\n\t" + "-o <listOfOperators>" + "\t" + "The space separated list of rmg.bro.operators to be used. Possible values are {dalal,satoh,winslett,forbus}";
                    out += "\n\t" + "-c <numberOfCylces>" + "\t" + "The number models that should be created given as an integer. Additionally the special value 'max' is allowed.\n\t\t\t\t"
                            + "If it is provided the number of possible combinations of models is calculated based on the atom count.\n\t\t\t\t"
                            + "NOTE: This is overwritten by the number of seed files.";
                    out += "\n\t" + "-m <minimumModelSize>" + "\t" + "Define the required minimum model sizes for revised models.";
                    out += "\n\t" + "-d [outputPath]" + "\t\t" + "Output path where seeds should be stored. If none is given a file with a generic name is written to the working directory.";
                    out += "\n\t" + "-p [filepath]" + "\t\t" + "Optionally save generated knowledge bases, revision models and results to a database file.\n\t\t\t\t"
                                  + "If no file/name is specified 'default.db' will be created in the current directory.";
                    out += "\n\t" + "-f" + "\t\t\t" + "Switch to control if an output should be generated if all revised models for the given rmg.bro.operators are in Horn logic.\n\t\t\t\t"
                            + "The default is to suppress this.";
                    out += "\n\t" + "-v" + "\t\t\t" + "Verbose output. Adds the original models for the knowledge base and the revision models to the output.\n\t\t\t\t"
                            + "If a second 'v' is appended: '-v' -> '-vv' seeds are added to the output.";
                    out += "\n\t" + "-q" + "\t\t\t" + "Suppress output.";
                    out += "\n\t" + "-h" + "\t\t\t" + "This help message.";
                    out += "\n\n\t" + "The default configuration is equal to: java rmg/bro/main/RMGRunner -k 3,5,20,5 -r 3,5,20,5 -o dalal forbus winslett satoh -c 1 -m 0";
                    System.out.println(out);
                }
                default -> System.err.println("Unknown argument "+arg);
            }
        }

        if(maxCycles) {
            cycles = maxCycles(kb,rm);
        }

        if(!seeds.isEmpty()) {
            cycles = seeds.size();
        }

        return new RMGConfig(kb,rm,operators,seeds,seedsFile);
    }

    private static Integer maxCycles(ModelConfig kb, ModelConfig rm) {
        int varKb = sumCombinations(kb.getAtomCnt(),kb.getMaxModelAtomCnt());
        int varRm = sumCombinations(rm.getAtomCnt(),rm.getMaxModelAtomCnt());
        return Math.max(varKb,varRm);
    }

    private static void writeSeeds(String path,int cycle, Queue<Long> kb, Queue<Long> mu) throws IOException, DirectoryNotCreatedException {
        String exceptMsg = "One or more directories in the given path could not be created.";
        String prefix = cycle + "_";
        String datFile = genericFileName("yyyyMMdd-HHmmssSSS") + "_seeds.dat";

        /* Check and prepare path */
        if(path == null) {
            path = "";
        } else if(path.charAt(path.length()-1) != File.separatorChar) {
            path += File.separator;
        }
        path += genericFileName("yyyyMMdd-HHmm") + "_seeds" + File.separator;

        File outPath = new File(path);
        if(!outPath.exists()) {
            boolean dirsCreated;
            if(path.charAt(path.length()-1) == File.separatorChar) {
                dirsCreated = outPath.mkdirs();
                path += prefix + datFile;
            } else {
                System.out.println(outPath.getPath());
                dirsCreated = new File(outPath.getPath()).mkdirs();
            }

            if(!dirsCreated) {
                throw new DirectoryNotCreatedException(exceptMsg);
            }
        } else if(outPath.isDirectory()) {
            if(!outPath.exists() && !outPath.mkdirs()) {
                throw new DirectoryNotCreatedException(exceptMsg);
            }
            path += prefix + datFile;
        } else if(!outPath.getPath().equals("")) {
            if(!outPath.exists() && !new File(outPath.getPath()).mkdirs()) {
                throw new DirectoryNotCreatedException(exceptMsg);
            }
            path = outPath.getPath() + File.separator + prefix + outPath.getName();
        }

        FileWriter myWriter = new FileWriter(path);
        for (Long seed : kb) {
            myWriter.write(seed.toString()+System.lineSeparator());
        }
        for (Long seed : mu) {
            myWriter.write(seed.toString()+System.lineSeparator());
        }
        myWriter.close();
    }

    private static String genericFileName(String pattern) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(cal.getTime());
    }

    private static boolean checkNextArg(String str) {
        return str.charAt(0) == '-';
    }

    private static Set<String> readOperators(ListIterator<String> iter) {
        Set<String> operators = new HashSet<>();

        while(iter.hasNext()) {
            String next = iter.next();
            if(checkNextArg(next)) {
                iter.previous();
                break;
            }
            operators.add(next);
        }
        return operators;
    }

    private static ModelConfig readModelConfig(ListIterator<String> iter) throws InvalidArgumentException {
        String[] split = new String[ModelConfig.PARAMS];
        while (iter.hasNext()) {
            String next = iter.next();
            if (checkNextArg(next)) {
                iter.previous();
                break;
            }
            switch (next.substring(0,3)) {
                case "min" -> split[0] = getArgValue(next,iter);
                case "max" -> split[1] = getArgValue(next,iter);
                case "cnt" -> split[2] = getArgValue(next,iter);
                case "mac" -> split[3] = getArgValue(next,iter);
                default -> {
                    split = next.split(",");
                    if(split.length > ModelConfig.PARAMS) {
                        throw new InvalidArgumentException("Too many parameters were given. Maximum number is "+ModelConfig.PARAMS);
                    }
                }
            }
        }

        Integer[] values = new Integer[ModelConfig.PARAMS];
        for (int i = 0; i < split.length; i++) {
            // For time optimization check out:
            // https://stackoverflow.com/questions/5439529/determine-if-a-string-is-an-integer-in-java
            try {
                // Possible null is ok here, because we want to fail in both cases,
                // when the string cannot be converted to an integer and if the integer is missing.
                values[i] = Integer.parseInt(split[i]);
            } catch (NumberFormatException e) {
                throw new InvalidArgumentException("Not a '=' separated key-value pair, and not an integer: "+split[i]);
            }
        }

        return new ModelConfig(values);
    }

    private static String getArgValue(String next, ListIterator<String> iter) {
//        return next.charAt(3) == '=' ? next.substring(3): iter.next();
        if(next.length() >= 4 && next.charAt(3) == '=') {
            if(next.length() > 4) {
                return next.substring(4);
            }
            return iter.next();
        }
        next = iter.next();
        if(next.charAt(0) == '=') {
            if(next.length() > 1) {
                return next.substring(1);
            }
            return iter.next();
        }

        return null;
    }

    private static Map<String,Queue<Long>> readFiles(String inputPath) throws IOException {
        Map<String,Queue<Long>> result = new HashMap<>();
        File file = new File(inputPath);

        if(file.isDirectory()) {
           File[] files = file.listFiles();
           if(files == null) {
               throw new IOException("Cannot list files of directory "+inputPath);
           }

            for (File f : files) {
                result.put(f.getName(),readFile(f));
            }
        } else {
            result.put(file.getName(),readFile(file));
        }
        return result;
    }

    private static Queue<Long> readFile(File f) throws FileNotFoundException {
        Queue<Long> seeds = new LinkedList<>();

        Scanner sc = new Scanner(f);

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            seeds.add(Long.valueOf(line));
//                seeds.push(Long.valueOf(line));
        }

        return seeds;
    }

    private static String getParameter(Iterator<String> it, String msg) throws InvalidArgumentException {
        String next;

        if(it.hasNext()) {
            next = it.next();

            if(next.charAt(0) != '-') {
                return next;
            }
        }

        throw new InvalidArgumentException(msg);
    }
}
