package rmg.bro.main;

import rmg.bro.dto.Interpretation;
import rmg.bro.dto.ModelConfig;
import rmg.bro.main.exceptions.InvalidConfigException;
import rmg.bro.main.exceptions.LetterOutOfBoundsException;
import rmg.bro.util.Pair;

import java.util.*;

public class RandomModelGenerator {


    /**
     *
     * @return
     */
    public static Pair<Set<Interpretation>, Queue<Long>> createModels(ModelConfig config, Queue<Long> seeds) throws LetterOutOfBoundsException, InvalidConfigException {

        int minModelCnt = config.getMinModelCnt();
        int maxModelCnt = config.getMaxModelCnt();
        int atomCnt = config.getAtomCnt();
        int maxModelAtomCnt = config.getMaxModelAtomCnt();

        if(minModelCnt > maxModelCnt || atomCnt < 1 || maxModelAtomCnt > atomCnt) {
            throw new InvalidConfigException("Lower bounds have to be smaller than upper bounds, " +
                    "the models has to have at least one atom, " +
                    "and the maximum number of atoms per model cannot be larger " +
                    "than the number of distinct atoms overall:\n" +
                    "minModelCnt("+minModelCnt+") <= maxModelCnt("+maxModelCnt+")\n" +
                    "atomCnt("+atomCnt+") >= 1\n" +
                    "maxModelAtomCnt("+maxModelAtomCnt+") <= atomCnt("+atomCnt+")");
        }
        Set<Interpretation> mu = new HashSet<>();

        Random rand = new Random();
        Random rSeed = new Random();
        Queue<Long> outputSeeds = new LinkedList<>();
        int rndPositiveAtoms, rndRmSize;

        long seed = 0;

//        do {
//            mu.clear();
//            outputSeeds.clear();

            seed = nextSeed(rSeed,seeds);
            outputSeeds.add(seed);
            rand.setSeed(seed);
            rndRmSize = rand.nextInt(maxModelCnt+1) + (maxModelCnt-minModelCnt) + 1;
            for(int j=0;j<rndRmSize;j++) {
                Interpretation ret;
                Set<String> atoms = new HashSet<>();


                do {
                    seed = nextSeed(rSeed, seeds);
                    outputSeeds.add(seed);
                    rand.setSeed(seed);
                    rndPositiveAtoms = rand.nextInt(maxModelAtomCnt + 1) + 1;
                } while(rndPositiveAtoms > atomCnt);

                for (int i = 0; i < rndPositiveAtoms; i++) {
                    int r;
                    String rStr;
                    do {
                        seed = nextSeed(rSeed, seeds);
                        outputSeeds.add(seed);
                        rand.setSeed(seed);
                        r = rand.nextInt(atomCnt);
                        rStr = getLetterFromInt(r);
                    } while (atoms.contains(rStr));
                    atoms.add(rStr);
                }


                final String[] interpretation = {""};
                atoms.forEach(p -> interpretation[0] += p);
                ret = new Interpretation(interpretation[0]);

                mu.add(ret);
            }
//        } while(!mu.equals(HornFunctions.closure(mu)));
        mu = HornFunctions.closure(mu);

        return new Pair<>(mu,outputSeeds);
    }

    /**
     *
     * @param rSeed A java random object used to get a fresh seed if the second argument @seeds is empty.
     * @param seeds A queue containing a list of seeds from a previous execution. This is used to reproduce previous results.
     * @return A seed which is used to generate a new random number in a Java Random object.
     */
    private static long nextSeed(Random rSeed, Queue<Long> seeds) {
        if(seeds.isEmpty()) {
            return rSeed.nextLong();
        }
        return seeds.poll();
    }

    /**
     *
     * @param i Input number to be converted to a lowercase letter if applicable.
     * @return A lowercase letter mapped from an integer according to the ASCII definition. NULL if the integer is out of range.
     */
    public static String getLetterFromInt(int i) throws LetterOutOfBoundsException {
        if(i < 0 || i >= 26) {
            throw new LetterOutOfBoundsException("Atom count is bound by the letters in the alphabet. Valid values are between 1 and 26, both inclusive.");
        }
        return String.valueOf((char)(i + 97)); // We are interested only in lower case letters
    }
}
