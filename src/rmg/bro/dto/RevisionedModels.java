package rmg.bro.dto;

import java.util.Set;

public class RevisionedModels {
    private final String operator;
    private final String fragment;
    private final boolean inFragment;
    private final Set<Interpretation> kb;
    private final Set<Interpretation> mu;
    private final Set<Interpretation> resultModels;

    public RevisionedModels(String operator,
                            String fragment,
                            boolean inFragment,
                            Set<Interpretation> kb,
                            Set<Interpretation> mu,
                            Set<Interpretation> resultModels) {
        this.operator = operator;
        this.fragment = fragment;
        this.inFragment = inFragment;
        this.kb = kb;
        this.mu = mu;
        this.resultModels = resultModels;
    }

    public String getOperator() {
        return operator;
    }

    public boolean isInFragment() {
        return inFragment;
    }

    public Set<Interpretation> getResultModels() {
        return resultModels;
    }

    public Set<Interpretation> getKb() {
        return kb;
    }

    public Set<Interpretation> getMu() {
        return mu;
    }

    public String getFragment() {
        return fragment;
    }

    @Override
    public String toString() {
        return operator + ": "
                + kb + " ° "
                + mu + " = "
                + resultModels + ";"
                + inFragment;
    }
}
