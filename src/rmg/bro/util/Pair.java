package rmg.bro.util;

import java.util.Objects;

public class Pair<E, F> {

    private E fst;
    private F snd;

    public Pair(E fst, F snd) {
        this.fst = fst;
        this.snd = snd;
    }

    public E getFst() {
        return fst;
    }

    public void setFst(E fst) {
        this.fst = fst;
    }

    public F getSnd() {
        return snd;
    }

    public void setSnd(F snd) {
        this.snd = snd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pair)) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return fst.equals(pair.fst) &&
                snd.equals(pair.snd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fst, snd);
    }
}