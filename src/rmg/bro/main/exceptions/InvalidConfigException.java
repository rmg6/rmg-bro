package rmg.bro.main.exceptions;

public class InvalidConfigException extends Throwable {
    public InvalidConfigException(String message) {
        super(message);
    }
}
