package rmg.bro.operators;

import rmg.bro.dto.Interpretation;
import rmg.bro.dto.ModelDiff;

import java.util.Set;

public class Dalal extends BeliefRevOp {

    /* [K ◦D µ] = {w ∈ [µ] | ∃u ∈ [K] such that w�u ∈ mincard([µ] � [K])} */
    @Override
    public Set<Interpretation> apply(Set<Interpretation> kb, Set<Interpretation> mu) {
        return revision(minCard(diam(mu,kb)));
    }

    @Override
    public String getName() {
        return "dalal";
    }
}
