package rmg.bro.operators;

import rmg.bro.dto.Interpretation;
import rmg.bro.dto.ModelDiff;

import java.util.HashSet;
import java.util.Set;

public abstract class BeliefRevOp {


    protected static Set<Interpretation> revision(Set<ModelDiff> minset) {
        Set<Interpretation> result = new HashSet<>();

        minset.forEach(p -> result.add(p.getRevision()));
        return result;
    }

    public abstract Set<Interpretation> apply(Set<Interpretation> kb, Set<Interpretation> mu);

    public static Set<ModelDiff> diam(Set<Interpretation> mu, Set<Interpretation> uKb) {
        Set<ModelDiff> result = new HashSet<>();


        for(Interpretation i1: mu) {
            for(Interpretation i2: uKb) {
                Interpretation i = i1.symDiff(i2);

                if(!i.INTERPRETATION.isEmpty()) {
                    result.add(new ModelDiff(i1,i2,i));
                }
            }
        }
        return result;
    }


    /*
     * - subset minimality (⊆):  We only accept solutionsS, s.t. no propersubset S′ ⊂ S is a solution.
     * - minimum cardinality (≤):  We only accept solutionsS, s.t. there does not exist a solution S′
     *   with fewer elements, i.e.,|S′|<|S|.
     */
    public static Set<ModelDiff> minSubset(Set<ModelDiff> interpretations) {
        Set<ModelDiff> minimal = new HashSet<>();

        Set<ModelDiff> ignore = new HashSet<>();

        for (ModelDiff d1 : interpretations) {
                for (ModelDiff d2 : interpretations) {

                    Interpretation i1 = d1.getDiff();
                    Interpretation i2 = d2.getDiff();

                    if (d1.getRevision().equals(d2.getRevision()) ||  i1.equals(i2)) continue;
                    Interpretation min = new Interpretation(i1);


                    min.INTERPRETATION.removeAll(i2.INTERPRETATION);

                    boolean notDistinct = !d1.getKb().equals(d2.getKb()) || i1.intersect(d1.getKb()).size() > 0;

                    if(ignore.contains(d1)) continue;

                    if(min.size() == i1.size() && notDistinct ) {
                        minimal.add(d1);
                    }

                    if ((min.INTERPRETATION.isEmpty()) && notDistinct) {
                        minimal.add(d1);
                        minimal.remove(d2);
                        ignore.add(d2);
                    }
                }
        }

        if(minimal.isEmpty()) {
            return interpretations;
        }
        return minimal;
    }

    public static Set<ModelDiff> minCard(Set<ModelDiff> interpretations) {
        Set<ModelDiff> result = new HashSet<>();

        int min = Integer.MAX_VALUE;

        for(ModelDiff diff: interpretations) {
            if(diff.getTrd().size() < min) {
                min = diff.getTrd().size();
            }
        }

        for(ModelDiff diff: interpretations) {
            if(diff.getTrd().size() == min) {
                result.add(diff);
            }
        }

        return result;
    }


    public abstract String getName();
}