package rmg.bro.main;

import rmg.bro.dto.Interpretation;

import java.util.HashSet;
import java.util.Set;

public class HornFunctions implements IPLFFunctions {

    public static Set<Interpretation> closure(Set<Interpretation> interpretations) {
        Set<Interpretation> result = new HashSet<>();
        for(Interpretation i1: interpretations) {
            for(Interpretation i2: interpretations) {
//                Interpretation i = new Interpretation(i1);
//                i.INTERPRETATION.retainAll(i2.INTERPRETATION);
//                result.add(i);
                result.add(i1.intersect(i2));
            }
        }
        return result;
    }
}
