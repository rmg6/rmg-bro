package rmg.bro.main;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import rmg.bro.dto.Interpretation;
import rmg.bro.operators.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class PaperExTest {
    private HashSet<Interpretation> mu;
    private HashSet<Interpretation> kb;

    private static Set<BeliefRevOp> operators;
    private static BeliefRevOp dalal;
    private static BeliefRevOp satoh;
    private static BeliefRevOp winslett;
    private static BeliefRevOp forbus;

    @BeforeAll
    static void setup() {
        operators = new HashSet<>();
        dalal = new Dalal();
        satoh = new Satoh();
        winslett = new Winslett();
        forbus = new Forbus();

        operators.addAll(Arrays.asList(dalal, satoh, winslett, forbus));


    }

    @Test
    public void ex1() {
        this.kb = new HashSet<>();
        this.kb.add(new Interpretation("abcd"));
        this.kb.add(new Interpretation("a"));

        this.mu = new HashSet<>();
        this.mu.add(new Interpretation("acd"));
        this.mu.add(new Interpretation("bd"));
        this.mu.add(new Interpretation("a"));

        Set<Interpretation> dalal = PaperExTest.dalal.apply(this.kb,this.mu);
        Set<Interpretation> satoh = PaperExTest.satoh.apply(this.kb,this.mu);
        Set<Interpretation> forbus = PaperExTest.forbus.apply(this.kb,this.mu);
        Set<Interpretation> winslett = PaperExTest.winslett.apply(this.kb,this.mu);

        System.out.println("dalal(kb,mu) = "+dalal);
        System.out.println("satoh(kb,mu) = "+satoh);
        System.out.println("forbus(kb,mu) = "+forbus);
        System.out.println("winslett(kb,mu) = "+winslett);

        System.out.println("Cl(kb) = "+HornFunctions.closure(this.kb));
        System.out.println("Cl(dalal(kb,mu)) = "+HornFunctions.closure(dalal));
    }

    @Test
    public void ex2() {
        this.mu = new HashSet<>();
        this.mu.add(new Interpretation("ab"));
//        this.mu.add(new Interpretation("b"));
        this.mu.add(new Interpretation("c"));
        this.mu.add(new Interpretation(""));

        this.kb = new HashSet<>();
        this.kb.add(new Interpretation("abc"));
        this.kb.add(new Interpretation("bcd"));

        Set<Interpretation> dalal = PaperExTest.dalal.apply(this.kb,this.mu);
        Set<Interpretation> satoh = PaperExTest.satoh.apply(this.kb,this.mu);
        Set<Interpretation> forbus = PaperExTest.forbus.apply(this.kb,this.mu);
        Set<Interpretation> winslett = PaperExTest.winslett.apply(this.kb,this.mu);

//        char c = "".chars().mapToObj(e -> (char)e).collect(Collectors.toList()).get(0);
//        System.out.println("character min value: "+c);

        System.out.println("dalal(kb,mu) = "+dalal);
        System.out.println("satoh(kb,mu) = "+satoh);
        System.out.println("forbus(kb,mu) = "+forbus);
        System.out.println("winslett(kb,mu) = "+winslett);

        System.out.println("Cl(kb) = "+HornFunctions.closure(kb));
        System.out.println("Cl(dalal(kb,mu)) = "+HornFunctions.closure(dalal));
    }

    @Test
    public void Ex2ImageTest() {
        HashSet<Interpretation> kb = new HashSet<>();
        HashSet<Interpretation> mu = new HashSet<>();

        kb.add(new Interpretation("abc"));

        mu.add(new Interpretation(""));
        mu.add(new Interpretation("a"));
        mu.add(new Interpretation("b"));
        mu.add(new Interpretation("c"));

        Set<Interpretation> resultD = dalal.apply(kb,mu);
        Set<Interpretation> resultS = satoh.apply(kb,mu);
        Set<Interpretation> resultW = winslett.apply(kb,mu);
        Set<Interpretation> resultF = forbus.apply(kb,mu);
        System.out.println("dalal:    "+resultD);
        System.out.println("satoh:    "+resultS);
        System.out.println("winslett: "+resultW);
        System.out.println("forbus:   "+resultF);

    }

    @Test
    public void ExTest() {
        HashSet<Interpretation> kb = new HashSet<>();
        HashSet<Interpretation> mu = new HashSet<>();

        kb.add(new Interpretation("abc"));
        kb.add(new Interpretation("bcd"));
        kb.add(new Interpretation("bc"));

        mu.add(new Interpretation("abcd"));
        mu.add(new Interpretation("ab"));
        mu.add(new Interpretation("cd"));
        mu.add(new Interpretation(""));

        Set<Interpretation> resultD = dalal.apply(kb,mu);
        Set<Interpretation> resultS = satoh.apply(kb,mu);
        Set<Interpretation> resultW = winslett.apply(kb,mu);
        Set<Interpretation> resultF = forbus.apply(kb,mu);
        System.out.println("dalal:    "+resultD);
        System.out.println("satoh:    "+resultS);
        System.out.println("winslett: "+resultW);
        System.out.println("forbus:   "+resultF);

    }
}
