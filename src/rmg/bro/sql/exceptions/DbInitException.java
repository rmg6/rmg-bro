package rmg.bro.sql.exceptions;

public class DbInitException extends Exception {
    public DbInitException(String message) {
        super(message);
    }
}
