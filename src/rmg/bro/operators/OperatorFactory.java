package rmg.bro.operators;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class OperatorFactory {

    private static Map<String,BeliefRevOp> operators;
    private static OperatorFactory instance;

    private OperatorFactory() {
        init();
    }

    private static void init() {
        if(operators == null) {
            operators = new HashMap<>();
        }

        if(operators.isEmpty()) {
            operators.put("dalal",new Dalal());
            operators.put("satoh",new Satoh());
            operators.put("winslett",new Winslett());
            operators.put("forbus", new Forbus());
        }
    }

    public OperatorFactory getInstance() {
        if(instance == null) {
            instance = new OperatorFactory();
        }
        return instance;
    }

    public static void registerOperator(String name, BeliefRevOp operator) {
        init();
        operators.put(name.toLowerCase(),operator);
    }

    public static Set<String> operators() {
        init();
        return operators.keySet();
    }

    public static BeliefRevOp getOperator(String operator) {
        init();
//        return rmg.bro.operators.get(op.toLowerCase());
        String op = operator.toLowerCase();

        if(operators.containsKey(op)) {
            return operators.get(op);
        }
        return null;
    }
}
