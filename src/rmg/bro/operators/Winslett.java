package rmg.bro.operators;

import rmg.bro.dto.Interpretation;

import java.util.HashSet;
import java.util.Set;

public class Winslett extends BeliefRevOp {

    /* [K ◦W µ] = {w ∈ [µ] | ∃u ∈ [K] such that w�u ∈ min⊆([µ] � u)} */
    @Override
    public Set<Interpretation> apply(Set<Interpretation> kb, Set<Interpretation> mu) {
        Set<Interpretation> result = new HashSet<>();

        for(Interpretation i: kb) {
            Set<Interpretation> u = new HashSet<>();
            u.add(i);
            result.addAll(revision(minSubset(diam(mu,u))));
        }
        return result;
    }

    @Override
    public String getName() {
        return "winslett";
    }
}
