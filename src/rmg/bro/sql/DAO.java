package rmg.bro.sql;

import rmg.bro.dto.Interpretation;
import rmg.bro.dto.RevisionedModels;
import rmg.bro.operators.*;
import rmg.bro.sql.exceptions.DbInitException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class DAO {

    protected Connection connection;

    public DAO(String path) throws SQLException, ClassNotFoundException, DbInitException {
        SqliteJDBC db = SqliteJDBC.getInstance();
        connection = db.getConnection(path == null ? "default.db" : path);

        init();
    }

    public void init() throws DbInitException {
        //TODO check if table exists in DB and output message
        //sqlite query: "SELECT name FROM sqlite_master WHERE type='table' AND name='{table_name}';"

        String table;
        PreparedStatement pstmt;
        try {
            //create tables
            table = "CREATE TABLE if not exists Models("
                    + "models_id integer primary key autoincrement"
                    + ",fragment text"
                    + ",comment text"
                    + ");";

            pstmt = connection.prepareStatement(table);
            pstmt.execute();
        } catch (SQLException e) {
            throw new DbInitException("Could not create Models table.\n"+e.getMessage());
        }

//        try {
//            pstmt = connection.prepareStatement("select * from Models");
//            pstmt.execute();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//            throw new DbInitException("Table Models does not exist yet.");
//        }

        try {
            table = "CREATE TABLE if not exists Results("
                    + "result_id integer PRIMARY KEY autoincrement"
                    + ",operator text NOT NULL"
                    + ",inFragment boolean"
                    + ",models_id integer"
                    + ",foreign key (models_id) references Models(models_id)"
                    + ");";

            pstmt = connection.prepareStatement(table);
            pstmt.execute();
    } catch (SQLException e) {
            throw new DbInitException("Could not create Results table.\n"+e.getMessage());
    }

        try {
    table = "CREATE TABLE if not exists KbModels("
                    + "model text NOT NULL"
                    + ",models_id integer"
                    + ",foreign key (models_id) references Models(models_id)"
                    + ");";

            pstmt = connection.prepareStatement(table);
            pstmt.execute();
} catch (SQLException e) {
            throw new DbInitException("Could not create KbModels table.\n"+e.getMessage());
        }

        try {
        table = "CREATE TABLE if not exists RevisionModels("
                    + "model text NOT NULL"
                    + ",models_id integer"
                    + ",foreign key (models_id) references Models(models_id)"
                    + ");";

            pstmt = connection.prepareStatement(table);
            pstmt.execute();
        } catch (SQLException e) {
            throw new DbInitException("Could not create RevisionModels table.\n"+e.getMessage());
        }

        try {
        table = "CREATE TABLE if not exists ResultModels("
                    + "model text NOT NULL"
                    + ",result_id integer"
                    + ",foreign key (result_id) references Results(result_id)"
                    + ");";

            pstmt = connection.prepareStatement(table);
            pstmt.execute();

        } catch (SQLException e) {
            throw new DbInitException("Could not create ResultModels table.\n"+e.getMessage());
        }
    }


    public void add(RevisionedModels revision) throws SQLException {
        String model = "insert into Models (fragment) values (?)";

        PreparedStatement pstmt = connection.prepareStatement(model);
        pstmt.setString(1,revision.getFragment());
        pstmt.execute();

        String selectModel = "select models_id " +
                                "from Models " +
                                "order by models_id desc " +
                                "limit 1;";
        pstmt = connection.prepareStatement(selectModel);
        ResultSet resultSet = pstmt.executeQuery();
        int modelsId = resultSet.getInt(1);

        insertModelSet(modelsId,"KbModels","models_id",revision.getKb());
        insertModelSet(modelsId,"RevisionModels","models_id",revision.getMu());

        String result = "insert into Results (operator,inFragment,models_id) values (?,?,?)";

        pstmt = connection.prepareStatement(result);
        pstmt.setString(1,revision.getOperator());
        pstmt.setBoolean(2,revision.isInFragment());
        pstmt.setInt(3,modelsId);
        pstmt.execute();

        selectModel = "select result_id " +
                "from Results " +
                "order by result_id desc " +
                "limit 1;";
        pstmt = connection.prepareStatement(selectModel);
        resultSet = pstmt.executeQuery();
        int resultId = resultSet.getInt(1);

        insertModelSet(resultId,"ResultModels","result_id",revision.getResultModels());

    }

    private void insertModelSet(int foreignKey, String table, String column, Set<Interpretation> models) throws SQLException {
        String stmt = "insert into "+table+" (model,"+column+") values (?,?)";

        PreparedStatement pstmt;

        for(Interpretation model: models) {
            pstmt = connection.prepareStatement(stmt);
            pstmt.setString(1,model.toString());
            pstmt.setInt(2,foreignKey);
            pstmt.execute();
        }

    }

    public Set<RevisionedModels> readRevisionResults() throws SQLException {
        BeliefRevOp D = new Dalal(), S = new Satoh(), W = new Winslett(), F = new Forbus();
        Set<RevisionedModels> result = new HashSet<>();

        String stmt = "select rm.model from  Results r join ResultModels rm on r.result_id = rm.result_id where r.operator = ?";
        PreparedStatement pstmt = connection.prepareStatement(stmt);

        for(String operator: new String[]{D.getName(),S.getName(),F.getName(),W.getName()}) {

        pstmt.clearParameters();
        pstmt.setString(1,operator);
        ResultSet rs = pstmt.executeQuery();

        Set<Interpretation> models = new HashSet<>();
        while(rs.next()) {
            models.add(new Interpretation(rs.getString(1)));
        }

        RevisionedModels rm = new RevisionedModels(operator,null,false,null,null,models);
        result.add(rm);
        }

        return result;
    }
}
