package rmg.bro.dto;

import rmg.bro.util.Pair;

import java.util.*;

public class RMGConfig {

    private final String seedsPath;
    private final ModelConfig kb;
    private final ModelConfig rm;
    private final LinkedHashMap<String, Queue<Long>> seeds;
    private final Set<String> operators;

/*
    public RMGConfig() {
        this.kb = new ModelConfig();
        this.rm = new ModelConfig();
        this.seeds = new LinkedList<>();
    }
*/

    public RMGConfig(ModelConfig kb, ModelConfig rm, Set<String> operators, Map<String, Queue<Long>> seeds, String seedsPath) {
        this.kb = kb;
        this.rm = rm;
        this.operators = operators;
        this.seeds = new LinkedHashMap<>(seeds);
        this.seedsPath = seedsPath;
    }

    public RMGConfig(RMGConfig input) {
        this.kb = new ModelConfig(input.kb);
        this.rm = new ModelConfig(input.rm);
        this.operators = new HashSet<>(input.operators);
        this.seeds = new LinkedHashMap<>(input.seeds);
        this.seedsPath = input.seedsPath;
    }

    public ModelConfig getKb() {
        return this.kb;
    }

    public ModelConfig getRm() {
        return this.rm;
    }

    public Set<String> getOperators() {
        return this.operators;
    }

    public Map<String, Queue<Long>> getSeeds() {
        return seeds;
    }

    public Pair<String,Queue<Long>> popSeeds() {
        if(this.seeds.isEmpty()) {
            return new Pair<>("",new LinkedList<>());
        }
         Map.Entry<String,Queue<Long>> e = this.seeds.entrySet().iterator().next();
         return new Pair<>(e.getKey(),e.getValue());
    }

    public String getSeedsPath() {
        return seedsPath;
    }
}
