package rmg.bro.dto;

public class ModelConfig {

    public static final int PARAMS = 4;

    private int minModelCnt;
    private int maxModelCnt;
    private int atomCnt;
    private int maxModelAtomCnt;

    public ModelConfig(int minModelCnt, int maxModelCnt, int atomCnt, int maxModelAtomCnt) {
        this.minModelCnt = minModelCnt;
        this.maxModelCnt = maxModelCnt;
        this.atomCnt = atomCnt;
        this.maxModelAtomCnt = maxModelAtomCnt;
    }

    public ModelConfig(Integer[] values) {
        this(values[0],values[1],values[2],values[3]);
    }

    public ModelConfig(ModelConfig config) {
        this(config.minModelCnt,config.maxModelCnt,config.atomCnt,config.maxModelAtomCnt);
    }

    public int getMinModelCnt() {
        return minModelCnt;
    }

    public void setMinModelSize(int minModelSize) {
        this.minModelCnt = minModelSize;
    }

    public int getMaxModelCnt() {
        return maxModelCnt;
    }

    public void setMaxModelCnt(int maxModelCnt) {
        this.maxModelCnt = maxModelCnt;
    }

    public int getAtomCnt() {
        return atomCnt;
    }

    public void setAtomCnt(int atomCnt) {
        this.atomCnt = atomCnt;
    }

    public int getMaxModelAtomCnt() {
        return maxModelAtomCnt;
    }

    public void setMaxModelAtomCnt(int maxModelAtomCnt) {
        this.maxModelAtomCnt = maxModelAtomCnt;
    }
}
