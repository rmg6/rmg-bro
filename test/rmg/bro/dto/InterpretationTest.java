package rmg.bro.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InterpretationTest {

    private Interpretation i1;
    private Interpretation i2;

    @BeforeEach
    public void setUp() {
        i1 = new Interpretation("acd");
        i2 = new Interpretation("abcd");

    }

    @Test
    public void symDiffSymmetric() {

        assertEquals(i1.symDiff(i2),i2.symDiff(i1));
    }

    @Test
    public void symDiffResult() {

        assertEquals(i1.symDiff(i2),new Interpretation("b"));
    }

    @Test
    public void symUnionResult() {

        assertEquals(i1.intersect(i2),new Interpretation("acd"));
    }

    @Test
    public void symDiffDistinct() {
        Interpretation i1 = new Interpretation("a");
        Interpretation i2 = new Interpretation("b");

        assertEquals(i1.symDiff(i2),new Interpretation("ab"));
    }
}