package rmg.bro.operators;

import rmg.bro.dto.Interpretation;

import java.util.HashSet;
import java.util.Set;

public class Forbus extends BeliefRevOp {

    /* [K ◦F µ] = {w ∈ [µ] | ∃u ∈ [K] such that w�u ∈ mincard([µ] � u)} */
    @Override
    public Set<Interpretation> apply(Set<Interpretation> kb, Set<Interpretation> mu) {
        HashSet<Interpretation> result = new HashSet<>();

        for(Interpretation i: kb) {
            HashSet<Interpretation> u = new HashSet<>();
            u.add(i);
            result.addAll(revision(minCard(diam(mu,u))));
        }
        return result;
    }

    @Override
    public String getName() {
        return "forbus";
    }
}
