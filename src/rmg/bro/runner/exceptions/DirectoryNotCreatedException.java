package rmg.bro.runner.exceptions;

public class DirectoryNotCreatedException extends Throwable {
    public DirectoryNotCreatedException(String msg) {
        super(msg);
    }
}
