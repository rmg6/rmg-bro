# rmg-bro

Random Model Generator (rmg) for belief revision rmg.bro.operators (bro)

Generator for random models to test the deviation of models from the Horn fragment 
of propositional logic after applying belief revision rmg.bro.operators.

The tool was created to collect information on the relationships between revision operators as discussed in the paper 
[Deviation in Belief Change on Fragments of Propositional Logic](https://dblp.org/rec/conf/ki/HaretW17) by Adrian Haret 
and Stefan Woltran. The paper presents proofs for some relationships. Others are said to be open for further 
research. Using this tool we want to find empirical evidence for (or against), the stated relationships. 
The idea here is to generate knowledge bases and their revisions randomly, analyze the data and check for which 
operators the revisions are subsets of others. The hope is that there is already evidence supporting our current belief 
using small knowledge bases and revision formulas.

## Conceptual View

Conceptually we are creating knowledge bases and revision formulae, which are assigned various interpretations. 
The revision operators are then applied to those sets of interpretations. The result sets are subsets of the models 
from the revision formulae.

For the analysis phase we collect the results of many revisions for each operator. These sets are then checked for their 
subset relations. 

## Implementation View

The generator does not create formulae and finds interpretations to them, but directly creates sets of interpretations. 
This shortcut is possible because the revision operators and relationships are defined in terms of models rather 
than formulae. The sets for the knowledge bases, and the models which are used in the revision operation are generated 
independently, so they may very well be disjunctive. 

Randomness is part of almost every aspect of the generation, be it the size of the knowledge base 
(resp. the set of revision models), model size, or the alphabet used. All, of course within the bounds of the passed 
arguments. Further if multiple knowledge bases are created, the order they are created in is also random, due to 
the concurrent execution of each cycle. This does not change the outcome as every cycle is completely independent of 
the others. It does help to speed up the generation though, especially when revisions that stay in Horn, or have size 1 
are filtered from the results. In those cases the number of cycles has to be higher to get reasonable results.

## Pre-requisites

The program was developed and tested using the following tools in the specified version:

- OpenJDK 14.0.2
- Ant 1.10.8
- GNU Make 4.3
- Sqlite3 3.33.0\*

For testing:

- Junit 5.4

\*The project contains a sqlite.jar which includes the necessary sqlite framework. 
It is also packaged into the `runner.jar` and `analyzer.jar` files to avoid additional external dependencies.

## Build from sources

- Clone the project and navigate to the directory `rmg-bro`.
- Run `make package`

## Running the program

When executing the above make directive, two jar files are crated:

1. `runner.jar`
2. `analyzer.jar`

They contain the same binaries but use different Main classes, namely *RMGRunner* and *RMGAnalyzer*.

For help, you can also run `make help` which will output the usage and all possible arguments 
for both the runner and the analyzer.

#### Program arguments

As already mentioned the command `make help` will output all possible arguments for both tools. The output should look like this:

```
Run the program as follows:

        java rmg/analyzer/RMGAnalyzer <Analysis1> [<Analysis2> ...] [-- <RMGRunnerArgument1> [RMGRunnerArgument2 ...]]

The double dash '--' has to be specified as a separator between analyzer and runner arguments.Available analyer options are:

        -knownRelations         Check the mathematically proven relations based on generated data.
        -forbusWinslett         Check the relation between the images of Forbus and Winslett operators.
        -dalalSatoh             Check the relation between the images of Dalal and Satoh operators.
        -db <filepath>          Allows to specify a (sqlite) database file to reuse previous results.
        -help [runner]          This help message. The optional argument 'runner' also displays the help message for the runner.
--------------------------------------------------------
Run the program as follows:

        java rmg/bro/main/RMGRunner [Arg1 ...]

        -s <seedsPath>         Input file or directory of input files, which hold seeds to reproduce previous outputs. 
                               Each file has to contain one seed per row, no other formatting, and the expected seeds are of Java datatype long.
        -k <knowledgeBaseCfg>  The config can be either of the following:
                                - A comma separated ordered list of integers with the definition 
                                  minModelCount,maxModelCount,numberOfAvailableAtoms,maxModelAtomCount (max number of atoms per model)
                                - A list of key-value pairs: min=<integer> max=<integer> cnt=<integer> mac=<integer>
        -r <revisionModelsCfg> Same as -k but for the models used to revise the knowledge base.
        -o <listOfOperators>   The space separated list of rmg.bro.operators to be used. Possible values are {dalal,satoh,winslett,forbus}
        -c <numberOfCylces>    The number models that should be created given as an integer. Additionally the special value 'max' is allowed.
                               If it is provided the number of possible combinations of models is calculated based on the atom count.
                               NOTE: This is overwritten by the number of seed files.
        -m <minimumModelSize>  Define the required minimum model sizes for revised models.
        -d [outputPath]        Output path where seeds should be stored. If none is given a file with a generic name is written to the working directory.
        -p [filepath]          Optionally save generated knowledge bases, revision models and results to a database file.
                               If no file/name is specified 'default.db' will be created in the current directory.
        -f                     Switch to control if an output should be generated if all revised models for the given rmg.bro.operators are in Horn logic. 
                               The default is to suppress this.
        -v                     Hash Verbose output. Adds the original models for the knowledgebase and the revision models to the output. 
                               If a second 'v' is appended: '-v' -> '-vv' seeds are added to the output.
        -q                     Suppress output.
        -h                     This help message.

        The default configuration is equal to: java rmg/bro/main/RMGRunner -k 3,5,20,5 -r 3,5,20,5 -o dalal forbus winslett satoh -c 1 -m 0
```
 **Note:** The help message shows the classpath rather than the *.jar files. It makes no difference though. 
 If you are using the compiled output directly you can run the program as shown above. When using the jar files 
 simply replace the classpath with the jar file, that's it.

The top part shows the arguments for *RMGAnalyzer* while the lower part shows the possible arguments for *RMGRunner*. 
In the following I will explain the arguments of the latter in detail.

### RMGRunner

This class can be executed with the command `java -jar runner.jar` plus some optional arguments. 
A full list of its arguments can be accessed by running `java -jar runner.jar -h`.

The default configuration generates one set of models as a knowledge base and one set of models 
for the revision of the knowledge base. Explicitly specified the default configuration looks as follows:

```
java -jar runner.jar -k 3,5,20,5 -r 3,5,20,5 -o dalal forbus winslett satoh -c 1 -m 0
```

The program outputs to *stdout* in a comma separated csv format, which can be easily captured 
by redirecting to a file, e.g. `java -jar runner.jar > outputFile.csv`. For reproducibility 
and possibly archiving there is an argument to output seeds into separate files in a folder. 
Properly capturing the seeds is more involved than capturing the output, 
therefore I implemented this directly.

#### Runner Arguments

The arguments for RMGRunner listed in the section **Program Arguments** are listed and explained here.

##### -s \<seedsPath>

To be able to reproduce a previous result, or a specific set of models (this is more complex, and I would not recommend it), 
it is possible to feed a list of seeds to the generator. Each seed is used in one call of the random number generator 
built into Java (class java.util.Random).

##### -k \<knowledgeBaseCfg>

The configuration for the knowledge base. The argument takes a comma separated list of size four as a parameter.
They configure the following:

1. Minimum number of models in the knowledge base,
2. Maximum number of models in the knowledge base,
3. Number of atoms in the alphabet, and
4. Maximum number of atoms per model.

It is also possible to specify a space separated list of <key>=<value> pairs. Note that no spaces are allowed around 
the '=' symbol. The keys are:

1. min
2. max
3. cnt
4. mac

##### -r \<revisionModelsCfg>

This configures the creation of the revision models. The mechanics are the same as for the knowledge bases.

##### -o \<listOfOperators>

With this parameter the operators, which are to be used during the program execution, can be configured. There are four 
pre-defined operators, namely **dalal**, **satoh**, **winslett**, and **forbus**. If others are to be added
their classes need to be added to the *operators* package and added to the list in the factory class in the same package. 

**Note:** A possible to-do is to load additional operators dynamically.

##### -c \<numberOfCycles>

Number of cycles --- or iterations --- to be executed. One cycle creates one set of knowledge bases and revision models.
For more in-depth analysis this is not sufficient. We could just call the program repeatedly from the shell, 
however that would require parsing the output which is unnecessary since we have all the objects already available.
Additionally, if the value `max` is given instead of an integer, the maximum number of cycles is calculated from the 
given knowledge base and model sizes. This is just the number of all possible combinations of a given alphabet.
The calculated number is of course just a rough estimation, because we do not check for duplicates in the 
resulting model sets. Thus, to actually cover all possible iterations either a larger number should be used, 
or duplicates should not increase the cycle counter.

This value is ignored when used together with the **-s** argument.

##### -m \<minimumModelSize>

In order to avoid trivial revisions with model sizes = 1, a minimum can be specified. Initially this was implemented to 
test the operator implementations. It may be useful for the analysis.

##### -d \<outputPath>

This argument is the dual to **-s** capturing the seeds generated during execution. For each cycle a file is generated,
where the cycle number is added to the file name. If no filename was given a generic one using the date and time is used. 
Regardless of the path a directory is created where all the seed files are stored.

##### -f

Controls whether revisions that are in Horn after the application of the revision operators should be displayed, or not. 
The default is to suppress these revisions.

##### -v

Controls the verbosity of the output. The default is to simply output the operator names, and their revision results. 
Using this option adds the original knowledge base and set of revision models. Adding a second *v* also adds the 
seeds to the output. 

The format for the output is a list of semi-colon separated entries, including a header. This way it can be saved
as a csv file and easily parsed by other applications.

##### -q 

Dual to **-v**, suppresses all output.

##### -h

The help message.

#### Example output

An example output of the runner:

```
operator;knowledge base (kb);revision models (rm);revisions;horn;kb seeds;rm seeds
forbus;[d, pdfl, f, h, df, ef, l, m, qdfhj, pbdgim, pd, hl, arc, km, {}];[dm, r, c, d, djk, pdhm, k, dh, rk, dfhlo, {}];[dm, r, c, d, pdhm, k, dh, dfhlo, {}];true;[6825896881694714672, -6144753795765869695, 4082750729313056401, -5343005396692870665, -5665693976234107057, -109626862395606336, 6800025947713375356, 6058217775924687548, 8540425177110624510, -6181048724758431082, 6445242372645179850, -7800930422072360425, 3685367660079432530, -5114364635177681735, -460665805806032324, -5156393703281958021, -6621208054438864045, 8633473004790267891, -4924395402493233470, 5271806880376640681, 1535390452231109247, -1042839241674819935, -9034895784793405436, -3387888517079651271, -6479672660584804028, 3820723738064938293, -9109143712924049188, 8998808706626265112, 2879833299938750857, -4036471743576600395, -6878719204853987110, -6052839674896278524];[-677109067778498377, 7427296303630764372, 5796636336098960271, -7619956986749553679, 8186194154218825482, -8850021532034301681, -5030282811342548622, 7489753757482894500, 6824402628848855959, 6327167366959361996, -4533302311246054431, 6912552747638420480, 1263279401655008932, 3271684659592717703, 6778678021603561400, -5947480687467556564, -2809789801119360697, -6969833769216231120, 7599467223091157992, 8065200550981610882, -3982557783942667283, 230238302460719917, 2346175788035178488, -4998452679304646895, -7705409262897247020, -3835209902495283243, -8790592107386770815, -1442021818805751013, -1615311125739032944, -6485604670046789131, -7157189726577432655, -3264837693034397544]
winslett;[d, pdfl, f, h, df, ef, l, m, qdfhj, pbdgim, pd, hl, arc, km, {}];[dm, r, c, d, djk, pdhm, k, dh, rk, dfhlo, {}];[dm, r, c, d, djk, pdhm, k, dh, dfhlo, rk, {}];true;[6825896881694714672, -6144753795765869695, 4082750729313056401, -5343005396692870665, -5665693976234107057, -109626862395606336, 6800025947713375356, 6058217775924687548, 8540425177110624510, -6181048724758431082, 6445242372645179850, -7800930422072360425, 3685367660079432530, -5114364635177681735, -460665805806032324, -5156393703281958021, -6621208054438864045, 8633473004790267891, -4924395402493233470, 5271806880376640681, 1535390452231109247, -1042839241674819935, -9034895784793405436, -3387888517079651271, -6479672660584804028, 3820723738064938293, -9109143712924049188, 8998808706626265112, 2879833299938750857, -4036471743576600395, -6878719204853987110, -6052839674896278524];[-677109067778498377, 7427296303630764372, 5796636336098960271, -7619956986749553679, 8186194154218825482, -8850021532034301681, -5030282811342548622, 7489753757482894500, 6824402628848855959, 6327167366959361996, -4533302311246054431, 6912552747638420480, 1263279401655008932, 3271684659592717703, 6778678021603561400, -5947480687467556564, -2809789801119360697, -6969833769216231120, 7599467223091157992, 8065200550981610882, -3982557783942667283, 230238302460719917, 2346175788035178488, -4998452679304646895, -7705409262897247020, -3835209902495283243, -8790592107386770815, -1442021818805751013, -1615311125739032944, -6485604670046789131, -7157189726577432655, -3264837693034397544]
dalal;[d, pdfl, f, h, df, ef, l, m, qdfhj, pbdgim, pd, hl, arc, km, {}];[dm, r, c, d, djk, pdhm, k, dh, rk, dfhlo, {}];[dm, r, c, d, k, dh, {}];true;[6825896881694714672, -6144753795765869695, 4082750729313056401, -5343005396692870665, -5665693976234107057, -109626862395606336, 6800025947713375356, 6058217775924687548, 8540425177110624510, -6181048724758431082, 6445242372645179850, -7800930422072360425, 3685367660079432530, -5114364635177681735, -460665805806032324, -5156393703281958021, -6621208054438864045, 8633473004790267891, -4924395402493233470, 5271806880376640681, 1535390452231109247, -1042839241674819935, -9034895784793405436, -3387888517079651271, -6479672660584804028, 3820723738064938293, -9109143712924049188, 8998808706626265112, 2879833299938750857, -4036471743576600395, -6878719204853987110, -6052839674896278524];[-677109067778498377, 7427296303630764372, 5796636336098960271, -7619956986749553679, 8186194154218825482, -8850021532034301681, -5030282811342548622, 7489753757482894500, 6824402628848855959, 6327167366959361996, -4533302311246054431, 6912552747638420480, 1263279401655008932, 3271684659592717703, 6778678021603561400, -5947480687467556564, -2809789801119360697, -6969833769216231120, 7599467223091157992, 8065200550981610882, -3982557783942667283, 230238302460719917, 2346175788035178488, -4998452679304646895, -7705409262897247020, -3835209902495283243, -8790592107386770815, -1442021818805751013, -1615311125739032944, -6485604670046789131, -7157189726577432655, -3264837693034397544]
satoh;[d, pdfl, f, h, df, ef, l, m, qdfhj, pbdgim, pd, hl, arc, km, {}];[dm, r, c, d, djk, pdhm, k, dh, rk, dfhlo, {}];[dm, r, c, d, k, dh, rk, {}];true;[6825896881694714672, -6144753795765869695, 4082750729313056401, -5343005396692870665, -5665693976234107057, -109626862395606336, 6800025947713375356, 6058217775924687548, 8540425177110624510, -6181048724758431082, 6445242372645179850, -7800930422072360425, 3685367660079432530, -5114364635177681735, -460665805806032324, -5156393703281958021, -6621208054438864045, 8633473004790267891, -4924395402493233470, 5271806880376640681, 1535390452231109247, -1042839241674819935, -9034895784793405436, -3387888517079651271, -6479672660584804028, 3820723738064938293, -9109143712924049188, 8998808706626265112, 2879833299938750857, -4036471743576600395, -6878719204853987110, -6052839674896278524];[-677109067778498377, 7427296303630764372, 5796636336098960271, -7619956986749553679, 8186194154218825482, -8850021532034301681, -5030282811342548622, 7489753757482894500, 6824402628848855959, 6327167366959361996, -4533302311246054431, 6912552747638420480, 1263279401655008932, 3271684659592717703, 6778678021603561400, -5947480687467556564, -2809789801119360697, -6969833769216231120, 7599467223091157992, 8065200550981610882, -3982557783942667283, 230238302460719917, 2346175788035178488, -4998452679304646895, -7705409262897247020, -3835209902495283243, -8790592107386770815, -1442021818805751013, -1615311125739032944, -6485604670046789131, -7157189726577432655, -3264837693034397544]

operator;knowledge base (kb);revision models (rm);revisions;horn;kb seeds;rm seeds
forbus;[a, e, f, g, acfhjk, h, k, psef, qgmn, s, t, aqten, th, rbskl, qn, {}];[b, f, ae, j, cstfko, k, o, q, fl, s, pqhk, bs, io, bfgj, qj, {}];[q, fl, b, s, bs, ae, f, j, k, o, {}];true;[625079996952922707, -1614844258643415166, -2767664168060424503, -24932480375190389, 6215874080582756592, 576902183626437692, 5043940817609395658, -5998992778374344009, -6952460887137485700, -8723675337929450224, 3539134345813120321, -7928574861139991705, 6413914756347816865, -4867106790459002824, 7727702753996269537, 5007924358942132464, -7078950269449696323, -4263140280821816380, 4579903721427635796, 5227909387262961342, -1318776649661299869, 5085928140453512216, 8978031939983671733, 657021714804770765, 9058453422760197338, 3277763551498463852, 405928782784169494, 1982420713760079150, 9007368360067360924, -9088285919873645749, 8931148413257241852, -4540838751774569336, -7214273897819107368, 4851937972046472468, -4966257406019381282, 8568006047992606356, -3437593288478411023, -528735750433409412, 6315805263156228777];[-3064092512369402821, -4400807248351274090, -675646325078833523, 8308775239038268651, -747502014003915404, -8514397964373746684, 8618551021682420858, 7889810688409260314, -1728386581467603835, -3905363366266426750, 662117827990755366, 3715593475540112159, 5251936956719896878, 2733113513333488663, 4472810331937475463, -8333385635569846840, 2826738045690171712, -4336637900488154115, 4783174811058090985, 4214806288417898844, -3994031124536633568, -4567820588973054429, 4001318023552417899, -6597684393672505837, 5492474473973942344, -4202684726042601045, -6185290589811636093, -7002131017313425767, 1421367550347680710, -8251996674745722003, 7060953275713831542, 6982919239672197775, 901672619190796476, -1625523313696141197, 2717553661307423201, 3386574256545403146]
winslett;[a, e, f, g, acfhjk, h, k, psef, qgmn, s, t, aqten, th, rbskl, qn, {}];[b, f, ae, j, cstfko, k, o, q, fl, s, pqhk, bs, io, bfgj, qj, {}];[b, f, ae, j, cstfko, k, o, q, fl, s, pqhk, bs, io, bfgj, qj, {}];true;[625079996952922707, -1614844258643415166, -2767664168060424503, -24932480375190389, 6215874080582756592, 576902183626437692, 5043940817609395658, -5998992778374344009, -6952460887137485700, -8723675337929450224, 3539134345813120321, -7928574861139991705, 6413914756347816865, -4867106790459002824, 7727702753996269537, 5007924358942132464, -7078950269449696323, -4263140280821816380, 4579903721427635796, 5227909387262961342, -1318776649661299869, 5085928140453512216, 8978031939983671733, 657021714804770765, 9058453422760197338, 3277763551498463852, 405928782784169494, 1982420713760079150, 9007368360067360924, -9088285919873645749, 8931148413257241852, -4540838751774569336, -7214273897819107368, 4851937972046472468, -4966257406019381282, 8568006047992606356, -3437593288478411023, -528735750433409412, 6315805263156228777];[-3064092512369402821, -4400807248351274090, -675646325078833523, 8308775239038268651, -747502014003915404, -8514397964373746684, 8618551021682420858, 7889810688409260314, -1728386581467603835, -3905363366266426750, 662117827990755366, 3715593475540112159, 5251936956719896878, 2733113513333488663, 4472810331937475463, -8333385635569846840, 2826738045690171712, -4336637900488154115, 4783174811058090985, 4214806288417898844, -3994031124536633568, -4567820588973054429, 4001318023552417899, -6597684393672505837, 5492474473973942344, -4202684726042601045, -6185290589811636093, -7002131017313425767, 1421367550347680710, -8251996674745722003, 7060953275713831542, 6982919239672197775, 901672619190796476, -1625523313696141197, 2717553661307423201, 3386574256545403146]
dalal;[a, e, f, g, acfhjk, h, k, psef, qgmn, s, t, aqten, th, rbskl, qn, {}];[b, f, ae, j, cstfko, k, o, q, fl, s, pqhk, bs, io, bfgj, qj, {}];[q, b, fl, s, bs, ae, f, j, k, o, {}];true;[625079996952922707, -1614844258643415166, -2767664168060424503, -24932480375190389, 6215874080582756592, 576902183626437692, 5043940817609395658, -5998992778374344009, -6952460887137485700, -8723675337929450224, 3539134345813120321, -7928574861139991705, 6413914756347816865, -4867106790459002824, 7727702753996269537, 5007924358942132464, -7078950269449696323, -4263140280821816380, 4579903721427635796, 5227909387262961342, -1318776649661299869, 5085928140453512216, 8978031939983671733, 657021714804770765, 9058453422760197338, 3277763551498463852, 405928782784169494, 1982420713760079150, 9007368360067360924, -9088285919873645749, 8931148413257241852, -4540838751774569336, -7214273897819107368, 4851937972046472468, -4966257406019381282, 8568006047992606356, -3437593288478411023, -528735750433409412, 6315805263156228777];[-3064092512369402821, -4400807248351274090, -675646325078833523, 8308775239038268651, -747502014003915404, -8514397964373746684, 8618551021682420858, 7889810688409260314, -1728386581467603835, -3905363366266426750, 662117827990755366, 3715593475540112159, 5251936956719896878, 2733113513333488663, 4472810331937475463, -8333385635569846840, 2826738045690171712, -4336637900488154115, 4783174811058090985, 4214806288417898844, -3994031124536633568, -4567820588973054429, 4001318023552417899, -6597684393672505837, 5492474473973942344, -4202684726042601045, -6185290589811636093, -7002131017313425767, 1421367550347680710, -8251996674745722003, 7060953275713831542, 6982919239672197775, 901672619190796476, -1625523313696141197, 2717553661307423201, 3386574256545403146]
satoh;[a, e, f, g, acfhjk, h, k, psef, qgmn, s, t, aqten, th, rbskl, qn, {}];[b, f, ae, j, cstfko, k, o, q, fl, s, pqhk, bs, io, bfgj, qj, {}];[b, ae, f, j, k, o, q, fl, s, bs, io, qj, {}];true;[625079996952922707, -1614844258643415166, -2767664168060424503, -24932480375190389, 6215874080582756592, 576902183626437692, 5043940817609395658, -5998992778374344009, -6952460887137485700, -8723675337929450224, 3539134345813120321, -7928574861139991705, 6413914756347816865, -4867106790459002824, 7727702753996269537, 5007924358942132464, -7078950269449696323, -4263140280821816380, 4579903721427635796, 5227909387262961342, -1318776649661299869, 5085928140453512216, 8978031939983671733, 657021714804770765, 9058453422760197338, 3277763551498463852, 405928782784169494, 1982420713760079150, 9007368360067360924, -9088285919873645749, 8931148413257241852, -4540838751774569336, -7214273897819107368, 4851937972046472468, -4966257406019381282, 8568006047992606356, -3437593288478411023, -528735750433409412, 6315805263156228777];[-3064092512369402821, -4400807248351274090, -675646325078833523, 8308775239038268651, -747502014003915404, -8514397964373746684, 8618551021682420858, 7889810688409260314, -1728386581467603835, -3905363366266426750, 662117827990755366, 3715593475540112159, 5251936956719896878, 2733113513333488663, 4472810331937475463, -8333385635569846840, 2826738045690171712, -4336637900488154115, 4783174811058090985, 4214806288417898844, -3994031124536633568, -4567820588973054429, 4001318023552417899, -6597684393672505837, 5492474473973942344, -4202684726042601045, -6185290589811636093, -7002131017313425767, 1421367550347680710, -8251996674745722003, 7060953275713831542, 6982919239672197775, 901672619190796476, -1625523313696141197, 2717553661307423201, 3386574256545403146]
```

Created using the following command:
```
java -jar runner.jar -k 3,5,20,5 -r 3,5,20,5 -o dalal forbus winslett satoh -c 2 -m 0 -f -vv
```

The output is a semi-colon separated csv table. This is the most verbose output possible, and each cycle has a separate 
block with headers. The columns are:

1. operator - revision operator used to produce the output in this entry
2. knowledge base
3. revision models
4. revisions - the result of applying the operator to the knowledge base and the revision models
5. horn - is the result in horn. this can vary depending on the input arguments, so it may be a different fragment
6. kb seeds - the seeds for the knowledge base
7. rm seeds - the seeds for the revision models

### RMGAnalyzer

The analyzer class is called using `java -jar analyzer.jar`. By default, it will display a help text 
showing all possible arguments. One of them *has* to be given to output a result. They will
run some analysis, which currently consists of checking subset relationships between various 
revision operators' results.

The configuration for RMGRunner can be passed after the argument `--` (two dashes). 
The dashes separate the analyzer arguments from the runner arguments in order to clearly 
separate which arguments belong to which class. For the same reason the analyzer arguments 
are always whole words, and the runner arguments single letters or abbreviations.

#### Analyzer Arguments

The analysis consists of checking the subset relationships of the images between the various revision operators. 
For some of these relationships proofs were made in the paper mentioned at the top of the page. 
However not all constellations are proven (yet), so some of the analysis tries to find empirical evidence for them.

For these analysis tasks it is necessary to collect the results of many generated revisions. 
We have two mechanisms for that:

1. Start from scratch and analyse the result directly, or
2. Collect the results in a database, and read it for the analysis.

Due to the fact that we cannot generate an infinite set in feasible time, the results from these analysis should be read
carefully and not taken as exact answers. This is self-evident when the analyzer is started multiple times and 
sometimes returning different answers for an analysis task. If this happens the generated subset of the image is such 
that proposed subset relationship does not hold, although this does not contradict the general relationship between 
the images.

Further analysis may also be implement to test the structure of the results to determine for which prerequisits the 
results of revision operations deviate from their fragment of propositional logic.

For example the tool can be run as 
```
java -jar analyzer.jar -knownRelations -- -k 3,5,20,5 -r 3,5,20,5 -o dalal forbus winslett satoh -c 1 -m 0
```

This will start RMGRunner and pass through all its arguments (everything after the double dash '--').
Afterwards the result is checked for the already known releationships between the images of the operators. 
The output goes to STDOUT.

##### -knownRelations
As I already mentioned there are relationships between the images of the operators which were proven mathematically.
This argument starts an analysis which checks these relationships. Namely they are

- Im_Horn (°D) !⊆ Im_Horn(°W) ∪ Im_Horn(°F), 
- Im_Horn (°S) !⊆ Im_Horn(°D) ∪ Im_Horn(°W) ∪ Im_Horn(°F),
- Im_Horn (°F) !⊆ Im_Horn(°D) ∪ Im_Horn(°S),
- Im_Horn (°W) !⊆ Im_Horn(°S) ∪ Im_Horn(°D),

where 'Im_Horn(x)' is the image of x in the Horn fragment, '°\*'  denotes the revision operator \* = \{D,S,F,W\}, 
'x1 !⊆ x2' denotes that x1 is *not* a subset of x2, and '∪' is the usual set union symbol.


##### -forbusWinslett
The relationship between the operators *Forbus* and *Winslett* is yet unknown. Therefore this analysis checks both:

- Im_Horn (°W) ⊆ Im_Horn(°F), and
- Im_Horn (°F) ⊆ Im_Horn(°W).

##### -dalalSatoh
The relationship between the operators *Dalal* and *Satoh* is also not sure yet. More specifically, whether 
`Im_Horn (°D) ⊆ Im_Horn(°S)` holds is unknown. For completeness, we also check whether `Im_Horn (°S) ⊆ Im_Horn(°D)`.

##### -db \<filepath\>
To be able to use the aforementioned database this argument can be used to specify a database file. 
Currently the implementation uses sqlite. 

##### -help [runner]
Displays all possible arguments and how the program needs to be run. 
If the `runner` parameter is present the help text for RMGRunner is also displayed.

#### Example output

The output of the tool has the following form:
```
Running generator...

--------------------------------------------------------------------------------
Building images...
Running analysis...
forbus subset winslett: true

winslett subset forbus: false
Explanation: [be, dh, bde, cde, ach, aci, bci, cfg, ceh, cei, cdj, bgh, bfi, cgi, ahj, dfj, dgi, efi, bij, fgi, cij, 
dij, ehj, abch, abci, abfg, adef, abeh, aceg, acdh, abcj, bcef, abdj, aceh, cdef, adeh, abej, bceh, bcdi, bcfh, aefg, 
adei, acgh, bcdj, befg, bcej, adej, acgi, aefi, cdei, bdgh, bcfj, bcgj, bchi, aefj, begh, cdgh, befi, cdfi, cegh, defh, 
cdgi, afgi, adij, begj, cdhi, aehj, afhi, behj, cehi, dfgi, bghi, beij, aghj, cfgj, cehj, cfhj, dfgj, cghi, deij, cghj, 
abcdf, abcdg, acdeg, abcej, acdfh, abcgi, acefh, abefi, abchi, adefh, bcdgh, acegh, abdgj, acdgi, abdhi, bcdgi, abfgi, 
bcdfj, bdefh, acdhi, cdefh, abehj, adefj, bcdhj, aceij, bdfgi, bdegj, cdfgh, bcfhi, bcfgj, abgij, adfhj, bdfhi, cdfgi, 
bcghi, acghj, cdehi, cdfgj, cefgi, bcfij, aeghi, cdehj, beghi, cefhi, aeghj, afghi, cdeij, defgi, bcgij, bchij, afghj, 
begij, dfghi, behij, bfhij, cfhij, cghij, eghij, abdefg, acdefg, abcegh, acdefh, abdegh, abcefj, abcdgj, abcfgi, bcdefh, 
abdfgh, abefgh, acdfgi, acdegj, acefgh, abdghi, acdehj, abefhi, bdefgh, acdeij, bcdehj, adefgi, acdfij, cdefgh, abefij, 
bcdghi, acdghj, bcefgj, abchij, bcefhi, abegij, bdefgj, bcefhj, acdgij, adeghi, bceghi, acfghj, adeghj, cdefgj, bcdgij, 
adfghj, bdfghi, bdegij, bcehij, defghi, defhij, bfghij, dfghij, abcdegh, abcdehi, abcdfgi, abdefgh, abcefhi, abcdghj, 
abcefhj, abdefgj, acdeghi, bcdefhj, acdfghj, abcfhij, bcdegij, abefgij, bcdfgij, bdefghi, abefhij, abeghij, cdefghj, 
bdefgij, bcdghij, befghij, defghij, abcdeghi, abdefghi, abcefhij, acdefgij, bcdefghj, abdeghij, adefghij]

dalal subset satoh: true

satoh subset dalal: false
Explanation: [bcdei, abcgj, abfgh, bdefg, adegh, abdhj, acehi, abdij, bcdhj, abghj, acghj, cefgh, befhi, cdfgj, bdghi, 
aeghj, cfghi, afhij, efghi, eghij, abcdeg, abcegi, abcefj, abdeij, adefgh, bcdehj, acdfij, cdefgi, acfghj, acdhij, 
adfghj, cdeghi, adehij, befghj, adghij, abcefgi, abcefhi, acdefgi, abdefgj, abcdgij, acdfghi, be, acdegij, acefghi, 
acefghj, ag, adefghi, acdfhij, abefhij, bcdehij, ci, bdeghij, aefghij, cdfghij, abcefghi, abcdfghj, abcdehij, abdefghj, 
adf, abdefgij, abcefhij, beg, bcdefhij, bcj, def, ceh, bei, bcdeghij, cdi, agj, cfi, cgh, dgh, cgj, dfj, bhj, dgi, chi, 
dgj, chj, fgi, ghi, ghj, hij, abcg, abch, acdg, abfg, abdj, acei, adfg, adeh, bdeg, acdj, bdfg, bdei, befh, cefg, abhj, 
acgj, afgh, defg, abij, cdej, aeij, bghj, cghi, dfij, bhij, fghj, ehij]
```

This was created by running:

```
java -jar analyzer.ja -dalalSatoh -forbusWinslett -- -k 1,5,10,10 -r 1,5,10,10 -q -c 100;
```

Tool shows whether the relationship holds or not. If not the models, that cause the relationship not to hold, are output
as an explanation.

## Portability and Cleanup

The jar files should be able to run anywhere with a JVM installed. The Java version has to be at least 14.
You don't need to install any extra packages or tools. For the usage of the predefined tests 
and the build script both ant and make have to be installed. The newest versions available on 
any major linux distribution should suffice, although I only tested them using the versions 
stated above. 

To remove all built binaries and jars simply execute `make clean`. This should remove all files 
that were created during the packaging or execution of the program.