all: basic

RMG_PATH = out/production/

RMG_BRO_RUN = rmg/bro/main/RMGRunner
RMG_ANALYZE_RUN = rmg/analyzer/RMGAnalyzer

RUNNER = java -jar runner.jar
ANALYZER = java -jar analyzer.jar

package:
	ant clean analyzer.package runner.package

help: runner.jar
	$(ANALYZER) -help; \
	echo "--------------------------------------------------------"; \
	$(RUNNER) -h

clean:
	rm -rf *_seeds *.dat *.db; \
	ant clean

basic: runner.jar
	$(RUNNER) -f

subset_rel1: analyzer.jar
	$(ANALYZER) -q -f

subset_rel2: analyzer.jar
	time $(ANALYZER) -k 3,5,26,26 -r 3,5,26,26 -f -q -d $(CURDIR) -c 10;
#	rm -f $@.out
#	cd $(RMG_PATH) ; \
#		cat ../../subset_rel2.test | while read par ; do \
#			echo $$par | cut -d' ' -f1 | tr -d "\n" >> ../../$@.out ; \
#			printf "   " >> ../../$@.out ; \
#			echo $$par | cut -d' ' -f2- | xargs java $(RMG_ANALYZE_RUN) | tr "\n" " " >> ../../$@.out ; \
#			printf "\n" >> ../../$@.out ; \
#		done

subset_rel3: analyzer.jar
	time $(ANALYZER) -knownRelations -- -k 1,5,15,15 -r 1,5,15,15 -f -q -c max;

subset_rel4: analyzer.jar
	time $(ANALYZER) -forbusWinslett -- -k 1,5,15,15 -r 1,5,15,15 -f -q -c max;

subset_rel5: analyzer.jar
	time $(ANALYZER) -forbusWinslett -db foo.db -- -k 1,7,5,5 -r 1,5,5,5 -q -c 200 -p foo.db;

subset_rel6: analyzer.jar
	time $(ANALYZER) -dalalSatoh -forbusWinslett -- -k 1,5,10,10 -r 1,5,10,10 -q -c 100;

subset_rel7: analyzer.jar
	time $(ANALYZER) -dalalSatoh -forbusWinslett -- -k 1,5,15,5 -r 1,5,15,5 -f -q -c 10 -d $(CURDIR) -s $(CURDIR)/20200929-1628_seeds;
