package rmg.bro.util;

public class Math {

    public static Integer sumCombinations(int n, int k) {
        int result = 0;
        for (int i = 0; i < k; i++) {
            result += binom(n,i+1);
        }
        return result;
    }

    public static long binom(long n, long k) {
        if(n < k) return 0;
        return fact(n)/(fact(n-k)*fact(k));
    }

    public static long fact(long k) {
        if(k == 0) return 1;
//        System.out.println((k*fact(k+sign(k)*(-1)))); ;
        return (k*fact(k+sign(k)*(-1)));
    }

    public static long sign(long k) {
        if(k == 0) return 0;
        return k < 0 ? -1 : 1;
    }
}
