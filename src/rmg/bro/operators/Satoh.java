package rmg.bro.operators;

import rmg.bro.dto.Interpretation;
import rmg.bro.dto.ModelDiff;

import java.util.Set;

public class Satoh extends BeliefRevOp {

    /* [K ◦S µ] = {w ∈ [µ] | ∃u ∈ [K] such that w�u ∈ min⊆([µ] � [K])} */
    @Override
    public Set<Interpretation> apply(Set<Interpretation> kb, Set<Interpretation> mu) {
        return revision(minSubset(diam(mu,kb)));
    }

    @Override
    public String getName() {
        return "satoh";
    }
}
