package rmg.bro.main.exceptions;

public class LetterOutOfBoundsException extends Throwable {
    public LetterOutOfBoundsException(String msg) {
        super(msg);
    }
}
