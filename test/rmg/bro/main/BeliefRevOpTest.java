package rmg.bro.main;

import rmg.bro.dto.Interpretation;
import rmg.bro.dto.ModelDiff;
import rmg.bro.dto.RevisionedModels;
import rmg.bro.operators.*;
import org.junit.jupiter.api.Test;


import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class BeliefRevOpTest {

    private HashSet<Interpretation> mu;
    private HashSet<Interpretation> kb;
    private Set<BeliefRevOp> operators;
    private BeliefRevOp dalal;
    private BeliefRevOp satoh;
    private BeliefRevOp winslett;
    private BeliefRevOp forbus;

    public BeliefRevOpTest() {

        this.operators = new HashSet<>();
        this.dalal = new Dalal();
        this.satoh = new Satoh();
        this.winslett = new Winslett();
        this.forbus = new Forbus();

        this.operators.addAll(Arrays.asList(dalal, satoh, winslett, forbus));

        this.mu = new HashSet<>();
        this.mu.add(new Interpretation("acd"));
        this.mu.add(new Interpretation("bd"));
        this.mu.add(new Interpretation("b"));

        this.kb = new HashSet<>();
        this.kb.add(new Interpretation("abcd"));
        this.kb.add(new Interpretation("a"));
    }

//    @BeforeEach
//    void setUp() {
//    }

    @Test
    public void applyDalal() {
        Set<Interpretation> result, check = new HashSet<>();
        result = this.dalal.apply(this.kb,this.mu);
        check.add(new Interpretation("acd"));

        assertEquals(result,check);
//        System.out.print("Dalal: "+result);
    }

    @Test
    public void applySatoh() {
        Set<Interpretation> result = this.satoh.apply(this.kb,this.mu), check = new HashSet<>();
        check.add(new Interpretation("acd"));
        check.add(new Interpretation("bd"));

        assertEquals(result,check);
//        System.out.print("Satoh: "+result);
    }

    @Test
    public void applyForbus() {
        Set<Interpretation> result = this.forbus.apply(this.kb,this.mu), check = new HashSet<>();
        check.add(new Interpretation("acd"));
        check.add(new Interpretation("b"));

        assertEquals(result,check);
//        System.out.print("Forbus: "+result);
    }

    @Test
    public void applyWinslett() {
        Set<Interpretation> result = this.winslett.apply(this.kb,this.mu), check = new HashSet<>();
        check.add(new Interpretation("acd"));
        check.add(new Interpretation("bd"));
        check.add(new Interpretation("b"));

        assertEquals(result,check);
//        System.out.print("Winslett: "+result);
    }

    @Test
    public void diam() {
        Set<ModelDiff> result = BeliefRevOp.diam(mu,kb), check = new HashSet<>();

        check.add(new ModelDiff(new Interpretation("acd"),new Interpretation("abcd"),new Interpretation("b")));
        check.add(new ModelDiff(new Interpretation("bd"),new Interpretation("abcd"),new Interpretation("ac")));
        check.add(new ModelDiff(new Interpretation("b"),new Interpretation("abcd"),new Interpretation("acd")));
        check.add(new ModelDiff(new Interpretation("acd"),new Interpretation("a"),new Interpretation("cd")));
        check.add(new ModelDiff(new Interpretation("bd"),new Interpretation("a"),new Interpretation("abd")));
        check.add(new ModelDiff(new Interpretation("b"),new Interpretation("a"),new Interpretation("ab")));

        assertEquals(result,check);

    }

    @Test
    public void minSubset() {
        HashSet<ModelDiff> result = new HashSet<>(), check = new HashSet<>();
        check.add(new ModelDiff(new Interpretation("acd"),new Interpretation("abcd"),new Interpretation("b")));
        check.add(new ModelDiff(new Interpretation("b"),new Interpretation("a"),new Interpretation("ab")));
        check.add(new ModelDiff(new Interpretation("bd"),new Interpretation("abcd"),new Interpretation("ac")));

        for(Interpretation i: kb) {
            HashSet<Interpretation> u = new HashSet<>();
            u.add(i);
            Set<ModelDiff> diam = BeliefRevOp.diam(mu, u);
            result.addAll(BeliefRevOp.minSubset(diam));

        }

        assertEquals(result,check);
    }

//    @Test
//    public void minSubsetGlobal() {
//        Set<ModelDiff> diam = rmg.bro.operators.BeliefRevOp.diam(mu, kb);
//        System.out.println(diam);
//    }

    @Test
    public void minCard() {

        Set<ModelDiff> result = new HashSet<>(), check = new HashSet<>();
        check.add(new ModelDiff(new Interpretation("acd"),new Interpretation("abcd"),new Interpretation("b")));
        check.add(new ModelDiff(new Interpretation("b"),new Interpretation("a"),new Interpretation("ab")));
        check.add(new ModelDiff(new Interpretation("acd"),new Interpretation("a"),new Interpretation("cd")));

        for(Interpretation i: kb) {
            HashSet<Interpretation> u = new HashSet<>();
            u.add(i);
            Set<ModelDiff> diam = BeliefRevOp.diam(mu, u);
            result.addAll(BeliefRevOp.minCard(diam));

        }


        assertEquals(result,check);
    }

    @Test
    public void randomTest1() {
//        HashSet<Interpretation> kb = new HashSet<>();
//        HashSet<Interpretation> mu = new HashSet<>();
//
//        kb.add(new Interpretation("im"));
//        kb.add(new Interpretation("m"));
//        kb.add(new Interpretation("qmo"));
//
//        mu.add(new Interpretation("qeujl"));
//        mu.add(new Interpretation("j"));
//        mu.add(new Interpretation("dfjk"));

        Set<Interpretation> resultD = this.dalal.apply(kb,mu);
        Set<Interpretation> resultS = this.satoh.apply(kb,mu);
        Set<Interpretation> resultW = this.winslett.apply(kb,mu);
        Set<Interpretation> resultF = this.forbus.apply(kb,mu);
        System.out.println("dalal:    "+resultD);
        System.out.println("satoh:    "+resultS);
        System.out.println("winslett: "+resultW);
        System.out.println("forbus:   "+resultF);

    }

    @Test
    public void subsetRelationTest() {
        String fragment = "horn";
        Map<Integer,Set<RevisionedModels>> models = new HashMap<>();
//        HashSet<Interpretation> kb = new HashSet<>();
//        HashSet<Interpretation> mu = new HashSet<>();
//
//        kb.add(new Interpretation("im"));
//        kb.add(new Interpretation("m"));
//        kb.add(new Interpretation("qmo"));
//
//        mu.add(new Interpretation("qeujl"));
//        mu.add(new Interpretation("j"));
//        mu.add(new Interpretation("dfjk"));
        Set<RevisionedModels> m1 = new HashSet<>();

        m1.add(new RevisionedModels("dalal",fragment,true,kb,mu,dalal.apply(kb,mu)));
        m1.add(new RevisionedModels("satoh",fragment,true,kb,mu,satoh.apply(kb,mu)));
        m1.add(new RevisionedModels("winslett",fragment,true,kb,mu,winslett.apply(kb,mu)));
        m1.add(new RevisionedModels("forbus",fragment,true,kb,mu,forbus.apply(kb,mu)));

        models.put(1,m1);
        for(Set<RevisionedModels> m: models.values()) {
            Set<Interpretation> dalal = null, forbus = null, winslett = null, satoh = null;

            for(RevisionedModels mm: m) {
                if(mm.getOperator().equals(this.dalal.getName())) dalal = this.dalal.apply(mm.getKb(),mm.getMu());
                if(mm.getOperator().equals(this.satoh.getName())) satoh = this.satoh.apply(mm.getKb(),mm.getMu());
                if(mm.getOperator().equals(this.winslett.getName())) winslett = this.winslett.apply(mm.getKb(),mm.getMu());
                if(mm.getOperator().equals(this.forbus.getName())) forbus = this.forbus.apply(mm.getKb(),mm.getMu());
            }

            if(dalal != null && forbus != null && winslett != null && satoh != null) {
                Set<Interpretation> unionWF = new HashSet<>();
                Set<Interpretation> unionDWF = new HashSet<>();
                Set<Interpretation> unionDS = new HashSet<>();
//                winslett.forEach(p -> unionWF.add(new Interpretation(p)));
                unionWF.addAll(winslett);
                unionWF.addAll(forbus);

                unionDWF.addAll(unionWF);
                unionDWF.addAll(dalal);

                unionDS.addAll(satoh);
                unionDS.addAll(dalal);

                System.out.println(dalal+" !sub "+unionWF+": "+!unionWF.containsAll(dalal));
                System.out.println(satoh+" !sub "+unionDWF+": "+!unionDWF.containsAll(satoh));
                System.out.println(forbus+" !sub "+unionDS+": "+!unionDS.containsAll(forbus));
                System.out.println(winslett+" !sub "+unionDS+": "+!unionDS.containsAll(winslett));
            }
            m.forEach(System.out::println);
            System.out.println("---");
        }
    }

    @Test
    public void inHornTest1() {
        this.kb.clear();
        this.kb.add(new Interpretation("abcd"));
        this.kb.add(new Interpretation("ab"));
        this.kb.add(new Interpretation("cd"));

        System.out.println("kb: "+this.kb);
        System.out.println("Cl(kb): "+HornFunctions.closure(this.kb));

        assert(!HornFunctions.closure(this.kb).equals(this.kb));
    }

    @Test
    public void inHornTest2() {
        this.kb.clear();
        this.kb.add(new Interpretation("abc"));
        this.kb.add(new Interpretation("bcd"));
        this.kb.add(new Interpretation("bc"));

        this.mu.clear();
        this.mu.add(new Interpretation("abcd"));
        this.mu.add(new Interpretation("ab"));
        this.mu.add(new Interpretation("cd"));
        this.mu.add(new Interpretation(""));

        System.out.println("kb: "+this.kb);
        System.out.println("Cl(kb): "+HornFunctions.closure(this.kb));

        System.out.println("mu: "+this.mu);
        System.out.println("Cl(mu): "+HornFunctions.closure(this.mu));



        assert(HornFunctions.closure(this.kb).equals(this.kb));
    }
}