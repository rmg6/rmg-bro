package rmg.bro.dto;

import rmg.bro.util.Triple;

public class ModelDiff extends Triple<Interpretation,Interpretation,Interpretation> {

    public ModelDiff(Interpretation fst, Interpretation snd, Interpretation trd) {
        super(fst, snd, trd);
    }

    public Interpretation getRevision() {
        return this.getFst();
    }

    public Interpretation getKb() {
        return this.getSnd();
    }

    public Interpretation getDiff() {
        return this.getTrd();
    }

    @Override
    public String toString() {
        return String.format("(%s Δ %s = %s)",this.getRevision(),this.getKb(),this.getDiff());
    }
}
