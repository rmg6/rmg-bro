CREATE TABLE if not exists Models(
    models_id integer primary key autoincrement
    ,fragment text
    ,comment text
);

CREATE TABLE if not exists Results(
    result_id integer PRIMARY KEY autoincrement
    ,operator text NOT NULL
    ,inFragment boolean
    ,models_id integer
    ,foreign key (models_id) references Models.models_id
);
