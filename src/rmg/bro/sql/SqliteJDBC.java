package rmg.bro.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqliteJDBC {

    private Connection connection;
    private static SqliteJDBC instance;

    private SqliteJDBC() {}

    public static SqliteJDBC getInstance() {
        if(SqliteJDBC.instance == null) {
            SqliteJDBC.instance = new SqliteJDBC();
        }
        return SqliteJDBC.instance;
    }

    public Connection getConnection(String path) throws ClassNotFoundException, SQLException {
        if(this.connection == null) {
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection("jdbc:sqlite:"+path);
        }
        return connection;
    }
}
