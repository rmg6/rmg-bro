package rmg.bro.dto;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Interpretation {
    public final Set<Character> INTERPRETATION;

    public Interpretation(String interpretation) {
        this.INTERPRETATION = new HashSet<>();

        this.INTERPRETATION.addAll(interpretation
                .chars()
                .mapToObj(e -> (char)e)
                .collect(Collectors.toList()));

    }

    public Interpretation(Interpretation ...interpretations) {
        this.INTERPRETATION = new HashSet<>();

        for(Interpretation i: interpretations) {
            this.INTERPRETATION.addAll(i.INTERPRETATION);
        }
    }

    public Interpretation symDiff(Interpretation other) {
        Interpretation iMuNoIKb = new Interpretation(this);
        Interpretation iKbNoIMu = new Interpretation(other);

        iMuNoIKb.INTERPRETATION.removeAll(other.INTERPRETATION);
        iKbNoIMu.INTERPRETATION.removeAll(this.INTERPRETATION);

        return new Interpretation(iMuNoIKb,iKbNoIMu);
    }

//    public Interpretation intersect(Interpretation other) {
//        Interpretation iMuNoIKb = new Interpretation(this);
//        Interpretation iKbNoIMu = new Interpretation(other);
//
//        iMuNoIKb.INTERPRETATION.retainAll(other.INTERPRETATION);
//        iKbNoIMu.INTERPRETATION.retainAll(this.INTERPRETATION);
//
//        return new Interpretation(iMuNoIKb,iKbNoIMu);
//    }

    public Interpretation intersect(Interpretation other) {
        Interpretation i = new Interpretation(this);
        i.INTERPRETATION.retainAll(other.INTERPRETATION);
        return i;
    }

    public int size() {
        return INTERPRETATION.size();
    }

    @Override
    public String toString() {
        final String[] str = {""};

        if(INTERPRETATION.isEmpty()) {
            str[0] = "{}";
        } else {
            INTERPRETATION.forEach(p -> str[0] += p);
        }

        return str[0];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Interpretation that = (Interpretation) o;
        return INTERPRETATION.equals(that.INTERPRETATION);
    }

    @Override
    public int hashCode() {
        return Objects.hash(INTERPRETATION);
    }
}
