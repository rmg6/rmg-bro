package rmg.bro.runner.exceptions;

public class InvalidArgumentException extends Throwable {
    public InvalidArgumentException(String msg) {
        super(msg);
    }
}
